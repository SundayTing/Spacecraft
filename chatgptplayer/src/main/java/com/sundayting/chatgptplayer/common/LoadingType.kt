package com.sundayting.chatgptplayer.common

enum class LoadingType {
    IDLE, LOADING
}