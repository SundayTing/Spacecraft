package com.sundayting.chatgptplayer.function.question

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sundayting.chatgptplayer.R

@Composable
@Preview
fun PreviewCommonToolBar() {
    CommonToolBar(title = "哈哈哈") {

    }
}

@Composable
fun CommonToolBar(
    title: String,
    onBack: () -> Unit,
) {

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.White)
            .padding(10.dp),
        contentAlignment = Alignment.CenterStart
    ) {
        Text(text = title, fontSize = 20.sp, modifier = Modifier.align(Alignment.Center))
        Image(
            painter = painterResource(id = R.drawable.ic_back),
            contentDescription = null,
            modifier = Modifier
                .size(30.dp)
                .clickable { onBack() }
        )
    }

}