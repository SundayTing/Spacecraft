package com.sundayting.chatgptplayer.function.image

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.chatgptplayer.common.LoadingType
import com.sundayting.chatgptplayer.function.image.bean.isFailed
import com.sundayting.chatgptplayer.function.image.bean.isSuccess
import com.sundayting.chatgptplayer.function.question.FinishOneShotEvent
import com.sundayting.chatgptplayer.function.question.OneShotEvent
import com.sundayting.chatgptplayer.function.question.ToastOneShotEvent
import com.sundayting.com.network.ktx.isNSuccess
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImageGenerationViewModel @Inject constructor(
    private val repo: ImageGenerationRepository,
) : ViewModel() {

    data class UiState(
        val inputText: String = "",
        val imgUrl: String? = null,
        val loadingType: LoadingType = LoadingType.IDLE,
        val countDown: Int = 0,
        val imageModel: ImageModel = ImageModel.STABLE_DIFFUSION,
        val predictingText: String = "",
    )

    enum class ImageModel(val modelName: String) {
        STABLE_DIFFUSION("stable-diffusion"),
        DALL_E("DALL·E"),
    }

    private val _oneShotEventFlow = MutableSharedFlow<OneShotEvent>()
    val oneShotEventFlow = _oneShotEventFlow.asSharedFlow()

    private val _uiStateFlow = MutableStateFlow(UiState())
    val uiStateFlow = _uiStateFlow.asStateFlow()

    fun onChangeModel(model: ImageModel) {
        _uiStateFlow.update { it.copy(imageModel = model) }
    }

    fun onBack() {
        viewModelScope.launch { _oneShotEventFlow.emit(FinishOneShotEvent) }
    }

    fun onTextInput(text: String) {
        viewModelScope.launch { _uiStateFlow.update { it.copy(inputText = text) } }
    }

    private var countDownJob: Job? = null
    private fun startCountDown(countDownSec: Int) {
        countDownJob?.cancel()
        _uiStateFlow.update { it.copy(countDown = countDownSec) }
        countDownJob = viewModelScope.launch {
            while (isActive && _uiStateFlow.value.countDown > 0) {
                delay(1000L)
                _uiStateFlow.update { it.copy(countDown = _uiStateFlow.value.countDown - 1) }
            }
        }
    }

    private var imageJob: Job? = null
    fun fetchImage() {
        imageJob?.cancel()
        _uiStateFlow.update {
            it.copy(
                loadingType = LoadingType.LOADING,
                imgUrl = null,
                predictingText = ""
            )
        }
        imageJob = viewModelScope.launch {
            val model = _uiStateFlow.value.imageModel
            if (model == ImageModel.DALL_E) {
                val result = repo.fetchOpenAIImage(_uiStateFlow.value.inputText)
                if (result.isNSuccess()) {
                    _uiStateFlow.update {
                        it.copy(
                            imgUrl = result.body.data.firstOrNull()?.url,
                        )
                    }
                    startCountDown(5)
                } else {
                    _oneShotEventFlow.emit(ToastOneShotEvent(result.failureReason.message))
                }
            } else if (model == ImageModel.STABLE_DIFFUSION) {
                val result = repo.predictImage(_uiStateFlow.value.inputText)
                if (result.isNSuccess()) {
                    startCountDown(20)
                    while (isActive) {
                        val delayResult = async { delay(500L) }
                        val predictDeferred = async { repo.getPredictedResult(result.body.id) }
                        val predictedResult = predictDeferred.await()
                        if (predictedResult.isNSuccess()) {
                            if (predictedResult.body.isSuccess) {
                                _uiStateFlow.update {
                                    it.copy(
                                        predictingText = predictedResult.body.logs.orEmpty(),
                                        imgUrl = predictedResult.body.output.firstOrNull().orEmpty()
                                    )
                                }
                                break
                            } else if (predictedResult.body.isFailed) {
                                _oneShotEventFlow.emit(ToastOneShotEvent("构建图片失败，请重试"))
                                break
                            } else {
                                _uiStateFlow.update {
                                    it.copy(
                                        predictingText = predictedResult.body.logs.orEmpty()
                                    )
                                }
                            }
                        } else {
                            _oneShotEventFlow.emit(ToastOneShotEvent(predictedResult.failureReason.message))
                            break
                        }
                        delayResult.await()
                    }
                } else {
                    _oneShotEventFlow.emit(ToastOneShotEvent(result.failureReason.message))
                }
            }
        }.also {
            it.invokeOnCompletion { _ ->
                _uiStateFlow.update { uiState ->
                    uiState.copy(loadingType = LoadingType.IDLE)
                }
            }
        }
    }

}