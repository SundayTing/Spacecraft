package com.sundayting.chatgptplayer.function.image

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.sundayting.chatgptplayer.function.question.FinishOneShotEvent
import com.sundayting.chatgptplayer.function.question.ToastOneShotEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ImageGenerationActivity : ComponentActivity() {

    private val viewModel by viewModels<ImageGenerationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ImageGenerationScreen()
        }

        lifecycleScope.launch {
            launch {
                viewModel.oneShotEventFlow.collect { event ->
                    when (event) {
                        is ToastOneShotEvent -> {
                            Toast.makeText(
                                this@ImageGenerationActivity,
                                event.content,
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        is FinishOneShotEvent -> {
                            finish()
                        }
                    }
                }
            }
        }
    }

}