package com.sundayting.chatgptplayer.function.service

import com.sundayting.chatgptplayer.function.image.bean.PredictResult
import com.sundayting.chatgptplayer.net.module.RetrofitModule
import com.sundayting.com.network.NResult
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ReplicateService {

    @POST("predictions")
    suspend fun predictImage(
        @Body body: ReplicateImageRequestBody,
    ): NResult<PredictResult>

    @GET("predictions/{prediction_id}")
    suspend fun getPredictedResult(
        @Path("prediction_id") id: String,
    ): NResult<PredictResult>

    companion object {
        private const val version =
            "f178fa7a1ae43a9a9af01b833b9d2ecf97b1bcb0acfd2dc5dd04895e042863f1"
    }

    data class ReplicateImageRequestBody(
        val input: Input,
        val version: String = ReplicateService.version,
    )

    data class Input(
        val prompt: String,
    )

}

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    fun provideService(
        @RetrofitModule.Replicate retrofit: Retrofit,
    ): ReplicateService {
        return retrofit.create(ReplicateService::class.java)
    }

}