package com.sundayting.chatgptplayer.function.image.bean

data class ImageResultBean(
    val created: Long = 0L,
    val data: List<ImageBean> = emptyList(),
)

data class ImageBean(
    val url: String = "",
)