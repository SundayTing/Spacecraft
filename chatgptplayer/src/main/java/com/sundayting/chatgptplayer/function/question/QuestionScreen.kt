package com.sundayting.chatgptplayer.function.question

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.sundayting.chatgptplayer.common.LoadingType
import com.sundayting.chatgptplayer.common.PrimaryColor
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun QuestionScreen(
    viewModel: QuestionViewModel = viewModel(),
) {
    val state by viewModel.uiStateFlow.collectAsState()

    QuestionContent(
        uiState = state,
        onClear = { viewModel.clear() },
        onClickRequest = { input ->
            viewModel.sendChatMessage(
                OpenAiService.ChatMessage(content = input)
            )
//            viewModel.startNewAnswer(
//                OpenAiService.Question(
//                    prompt = input,
//                    stream = true,
//                    temperature = 0.5f,
//                    maxTokens = 2048
//                )
//            )
        },
        onTextInput = { text ->
            viewModel.onInputText(text)
        },
        onCopyResult = { result ->
            viewModel.onCopy(result)
        },
        onBack = {
            viewModel.onBack()
        }
    )

}

@OptIn(ExperimentalComposeUiApi::class, ExperimentalFoundationApi::class)
@Composable
private fun QuestionContent(
    uiState: QuestionViewModel.UiState,
    onBack: () -> Unit = {},
    onClear: () -> Unit = {},
    onClickRequest: (String) -> Unit = {},
    onTextInput: (String) -> Unit = {},
    onCopyResult: (String) -> Unit = {},
) {

    val state by rememberUpdatedState(newValue = uiState)

    val keyboardController = LocalSoftwareKeyboardController.current

    val lazyState = rememberLazyListState()
    val qaList = state.qaBeanList
    LaunchedEffect(key1 = qaList.size) {
        lazyState.animateScrollToItem(qaList.lastIndex.coerceAtLeast(0))
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.White)
    ) {
        CommonToolBar(title = "你问我答") { onBack() }
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 30.dp),
            state = lazyState
        ) {
            item {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Spacer(modifier = Modifier.size(10.dp))
                    Text(
                        "模型：gpt-3.5-turbo-0301",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(align = Alignment.End),
                        style = TextStyle(color = Color.Gray.copy(alpha = 0.9f))
                    )
                }
            }
            stickyHeader {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        value = state.inputText,
                        onTextInput,
                        placeholder = {
                            Text("提一个问题", style = TextStyle(fontSize = 20.sp))
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 20.dp)
                            .height(80.dp),
                        textStyle = TextStyle(fontSize = 20.sp),
                        maxLines = 4,
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            focusedBorderColor = PrimaryColor,
                            cursorColor = PrimaryColor
                        )
                    )
                    LaunchedEffect(key1 = state.qaBeanList) {
                        lazyState.animateScrollToItem((state.qaBeanList.size - 1).coerceAtLeast(0))
                    }
                    Spacer(modifier = Modifier.size(15.dp))
                    Button(
                        onClick = {
                            onClickRequest(state.inputText)
                            keyboardController?.hide()
                        },
                        enabled = state.loadingType == LoadingType.IDLE && state.inputText.isNotBlank(),
                        colors = ButtonDefaults.buttonColors(backgroundColor = PrimaryColor)
                    ) {
                        Text("向ChatGpt发起提问", color = Color.White)
                    }
                    Spacer(modifier = Modifier.size(15.dp))
                }
            }
            items(items = qaList, key = { bean -> bean.time }) { bean ->
                val firstTextStyle = remember {
                    TextStyle(fontSize = 17.sp)
                }
                val contentTextStyle = remember {
                    TextStyle(fontSize = 15.sp)
                }
                if (bean.isMe) {
                    Row {
                        Text(text = "你：", style = firstTextStyle)
                        Surface(
                            color = Color.Gray.copy(alpha = 0.1f),
                            shape = RoundedCornerShape(15.dp),
                        ) {
                            Text(
                                text = bean.content,
                                style = contentTextStyle,
                                modifier = Modifier
                                    .padding(10.dp)
                            )
                        }

                    }

                } else {
                    Row(
                        horizontalArrangement = Arrangement.End,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Surface(
                            color = Color.Gray.copy(alpha = 0.1f),
                            shape = RoundedCornerShape(15.dp),
                            modifier = Modifier.weight(1f, false)
                        ) {
                            Text(
                                text = bean.content,
                                style = contentTextStyle,
                                modifier = Modifier
                                    .padding(10.dp)
                                    .pointerInput(Unit) {
                                        detectTapGestures(
                                            onLongPress = {
                                                if (state.loadingType != LoadingType.LOADING) {
                                                    onCopyResult(state.qaBeanList.last().content)
                                                }
                                            }
                                        )
                                    }
                            )
                        }

                        Text(text = "：机器人", style = firstTextStyle)
                    }
                }
                Spacer(modifier = Modifier.size(20.dp))
            }
            item {
                var loadingText by remember { mutableStateOf("加载中") }
                val loadingEnd = remember { listOf(".", "..", "...") }
                var currentIndex by remember { mutableStateOf(0) }
                if (state.loadingType == LoadingType.LOADING) {
                    LaunchedEffect(key1 = Unit) {
                        while (isActive) {
                            delay(200L)
                            loadingText = "加载中" + loadingEnd[currentIndex]
                            currentIndex = (currentIndex + 1) % loadingEnd.size
                        }
                    }
                    Box(
                        modifier = Modifier.fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(text = loadingText, fontSize = 20.sp, color = Color.Black)
                    }
                } else if (state.qaBeanList.isNotEmpty()) {
                    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                        Button(
                            onClick = {
                                onClear()
                                keyboardController?.hide()
                            },
                            colors = ButtonDefaults.buttonColors(backgroundColor = PrimaryColor)
                        ) {
                            Text("清空", color = Color.White)
                        }
                    }

                }

            }

        }
    }


}