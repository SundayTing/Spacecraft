package com.sundayting.chatgptplayer.function.question

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class QuestionActivity : ComponentActivity() {

    private val viewModel by viewModels<QuestionViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            viewModel.oneShowEventFlow.collect { event ->
                when (event) {
                    is ToastOneShotEvent -> {
                        Toast.makeText(this@QuestionActivity, event.content, Toast.LENGTH_SHORT)
                            .show()
                    }

                    is FinishOneShotEvent -> {
                        finish()
                    }
                }
            }
        }
        setContent {
            QuestionScreen()
        }
    }
}