package com.sundayting.chatgptplayer.function.question.repo

import androidx.annotation.StringDef
import com.google.gson.annotations.SerializedName
import com.sundayting.chatgptplayer.function.image.bean.ImageResultBean
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService.Role.Companion.ASSISTANT
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService.Role.Companion.SYSTEM
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService.Role.Companion.USER
import com.sundayting.chatgptplayer.net.module.RetrofitModule.OpenAI
import com.sundayting.com.network.NResult
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Streaming


interface OpenAiService {

    @POST("completions")
    @Streaming
    suspend fun startNewAnswer(@Body question: Question): ResponseBody

    data class Question(
        val prompt: String,
        @SerializedName("max_tokens")
        val maxTokens: Int = 4000,
        val model: String = "text-davinci-003",
        val temperature: Float = 0.2f,
        @SerializedName("top_p")
        val topP: Int = 1,
        val user: String = "yang123456",
        @SerializedName("frequency_penalty")
        val frequencyPenalty: Float = 0.0f,
        @SerializedName("presence_penalty")
        val presencePenalty: Float = 0.6f,
        val stream: Boolean = true,
    )

    @POST("images/generations")
    suspend fun fetchImage(@Body imageParam: ImageParam): NResult<ImageResultBean>

    data class ImageParam(
        val prompt: String,
        val user: String = "yang123456",
        val n: Int = 1,
        val size: String = "256x256",
    )

    @POST("chat/completions")
    @Streaming
    suspend fun sendChatContent(@Body content: ChatContent): ResponseBody

    @Retention(AnnotationRetention.SOURCE)
    @StringDef(SYSTEM, USER, ASSISTANT)
    annotation class Role {
        companion object {
            const val SYSTEM = "system"
            const val USER = "user"
            const val ASSISTANT = "assistant"
        }
    }

    data class ChatMessage(
        val content: String,
        @Role val role: String = USER
    )

    data class ChatContent(
        val messages: List<ChatMessage>,
        @SerializedName("max_tokens")
        val maxTokens: Int = 2048,
        val model: String = "gpt-3.5-turbo-0301",
        val temperature: Float = 0.7f,
        @SerializedName("top_p")
        val topP: Int = 1,
        val user: String = "yang123456",
        @SerializedName("frequency_penalty")
        val frequencyPenalty: Float = 0.0f,
        @SerializedName("presence_penalty")
        val presencePenalty: Float = 0.0f,
        val stream: Boolean = true,
    )

}

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    fun provideService(
        @OpenAI retrofit: Retrofit,
    ): OpenAiService {
        return retrofit.create(OpenAiService::class.java)
    }

}