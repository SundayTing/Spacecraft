package com.sundayting.chatgptplayer.function.question.repo

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuestionRepository @Inject constructor(
    private val openAiService: OpenAiService,
) {

    suspend fun startNewAnswer(question: OpenAiService.Question) =
        openAiService.startNewAnswer(question)

    suspend fun sendChatContent(content: OpenAiService.ChatContent) =
        openAiService.sendChatContent(content)

}