package com.sundayting.chatgptplayer.function.image.bean

data class PredictResult(
    val id: String = "",
    val logs: String? = "",
    val output: List<String> = emptyList(),
    val status: String = "",
)

fun PredictResult.getPrediction(): Prediction {

    return when (status) {
        "starting" -> Prediction.STARTING
        "processing" -> Prediction.PROCESSING
        "succeeded" -> Prediction.SUCCEEDED
        "failed" -> Prediction.FAILED
        "canceled" -> Prediction.CANCELED
        else -> Prediction.NONE
    }

}

val PredictResult.isSuccess: Boolean
    get() = getPrediction() == Prediction.SUCCEEDED

val PredictResult.isFailed: Boolean
    get() = getPrediction() == Prediction.FAILED || getPrediction() == Prediction.CANCELED

enum class Prediction {

    STARTING,
    PROCESSING,
    SUCCEEDED,
    FAILED,
    CANCELED,
    NONE

}