package com.sundayting.chatgptplayer.function.image

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.sundayting.chatgptplayer.R
import com.sundayting.chatgptplayer.common.LoadingType
import com.sundayting.chatgptplayer.common.PrimaryColor
import com.sundayting.chatgptplayer.function.image.ImageGenerationViewModel.ImageModel
import com.sundayting.chatgptplayer.function.question.CommonToolBar

@Composable
fun ImageGenerationScreen(
    viewModel: ImageGenerationViewModel = viewModel(),
) {
    val state by viewModel.uiStateFlow.collectAsState()
    ImageGenerationContent(
        state = state,
        onBack = { viewModel.onBack() },
        onTextInput = { viewModel.onTextInput(it) },
        onClickRequest = { viewModel.fetchImage() },
        onChangeModel = { viewModel.onChangeModel(it) }
    )

}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ImageGenerationContent(
    state: ImageGenerationViewModel.UiState,
    onBack: () -> Unit = {},
    onTextInput: (String) -> Unit = {},
    onClickRequest: () -> Unit = {},
    onChangeModel: (ImageModel) -> Unit = {},
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .background(color = Color.White)
            .padding(horizontal = 20.dp)
    ) {
        CommonToolBar(title = "图片生成") { onBack() }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {

            Column {
                var expanded by remember { mutableStateOf(false) }
                Button(
                    onClick = { expanded = !expanded }, colors = ButtonDefaults.buttonColors(
                        backgroundColor = PrimaryColor,
                        contentColor = Color.White
                    )
                ) {
                    Text("切换图片生成模型")
                }
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }) {
                    DropdownMenuItem(onClick = {
                        expanded = false
                        onChangeModel(ImageModel.STABLE_DIFFUSION)
                    }) {
                        Text(text = "stable-diffusion「来自Replicate」")
                    }
                    DropdownMenuItem(onClick = {
                        expanded = false
                        onChangeModel(ImageModel.DALL_E)
                    }) {
                        Text(text = "DALL·E「来自openAI」")
                    }
                }
            }

            Spacer(modifier = Modifier.size(10.dp))

            Surface(elevation = 5.dp, shape = RoundedCornerShape(6.dp)) {
                Text(
                    state.imageModel.modelName,
                    fontSize = 20.sp,
                    color = Color.Black,
                    modifier = Modifier.padding(horizontal = 6.dp, vertical = 2.dp)
                )
            }

        }
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            OutlinedTextField(
                value = state.inputText,
                onValueChange = { text ->
                    onTextInput(text)
                },
                placeholder = {
                    Text(
                        "描述你想绘制的图片(建议英文)",
                        style = TextStyle(fontSize = 20.sp)
                    )
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 10.dp)
                    .height(80.dp),
                textStyle = TextStyle(fontSize = 18.sp),
                maxLines = 4,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = PrimaryColor,
                    cursorColor = PrimaryColor
                )
            )
            Spacer(modifier = Modifier.size(15.dp))
            Button(
                onClick = {
                    onClickRequest()
                    keyboardController?.hide()
                },
                enabled = state.loadingType == LoadingType.IDLE && state.inputText.isNotBlank() && state.countDown <= 0,
                colors = ButtonDefaults.buttonColors(backgroundColor = PrimaryColor)
            ) {
                val buttonText = remember(state.loadingType, state.countDown) {
                    if (state.loadingType == LoadingType.LOADING) {
                        "生成图片中,请稍后..."
                    } else if (state.countDown > 0) {
                        "等待时间：${state.countDown}秒"
                    } else {
                        "生成图片"
                    }
                }
                Text(
                    text = buttonText,
                    color = Color.White
                )
            }
            Spacer(modifier = Modifier.size(15.dp))

            val scrollStateTwo = rememberScrollState()
            if (state.predictingText.isNotEmpty()) {
                LaunchedEffect(key1 = state.predictingText) {
                    scrollStateTwo.animateScrollTo(scrollStateTwo.maxValue)
                }
                Surface(elevation = 4.dp, shape = RoundedCornerShape(10.dp)) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(100.dp)
                            .padding(10.dp)
                            .verticalScroll(scrollStateTwo)
                    ) {
                        Text(text = state.predictingText, modifier = Modifier.fillMaxWidth())
                    }
                }
                Spacer(modifier = Modifier.size(10.dp))
            }
            if (state.imgUrl != null) {
                SubcomposeAsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(state.imgUrl)
                        .crossfade(true)
                        .diskCachePolicy(CachePolicy.ENABLED)
                        .build(),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f)
                        .clip(RoundedCornerShape(15.dp)),
                    contentScale = ContentScale.FillWidth
                ) {
                    when (painter.state) {
                        is AsyncImagePainter.State.Loading -> {
                            Box(
                                contentAlignment = Alignment.Center,
                                modifier = Modifier.fillMaxSize()
                            ) {
                                CircularProgressIndicator(
                                    color = PrimaryColor,
                                    modifier = Modifier
                                        .fillMaxSize()
                                        .padding(50.dp)
                                )
                                Text("图片加载中，请稍后...")
                            }

                        }

                        is AsyncImagePainter.State.Error -> {
                            Box(
                                contentAlignment = Alignment.Center
                            ) {
                                Column(
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Image(
                                        painter = painterResource(id = R.drawable.ic_error),
                                        contentDescription = null,
                                        modifier = Modifier.size(50.dp)
                                    )
                                    Spacer(modifier = Modifier.size(15.dp))
                                    Text("图片加载失败")
                                }
                            }


                        }

                        else -> {
                            SubcomposeAsyncImageContent()
                        }
                    }
                }
            }
        }
    }
}