package com.sundayting.chatgptplayer.function.question

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.chatgptplayer.common.LoadingType
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService
import com.sundayting.chatgptplayer.function.question.repo.QuestionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okio.Buffer
import org.json.JSONObject
import javax.inject.Inject

@HiltViewModel
class QuestionViewModel @Inject constructor(
    private val repo: QuestionRepository,
    @ApplicationContext context: Context,
) : ViewModel() {

    private val _uiStateFlow = MutableStateFlow(UiState())
    val uiStateFlow = _uiStateFlow.asStateFlow()

    private val _oneShotEventFlow = MutableSharedFlow<OneShotEvent>()
    val oneShowEventFlow = _oneShotEventFlow.asSharedFlow()

    private val clipboardManager =
        context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

    data class UiState(
        val inputText: String = "",
        val loadingType: LoadingType = LoadingType.IDLE,
        val qaBeanList: SnapshotStateList<QABean> = mutableStateListOf(),
    )

    data class QABean(
        val content: String,
        val isMe: Boolean,
        val time: Long,
    )

    fun onInputText(text: String) {
        _uiStateFlow.update { it.copy(inputText = text) }
    }

    fun clear() {
        _uiStateFlow.value.qaBeanList.clear()
    }

    fun onCopy(result: String) {
        clipboardManager.setPrimaryClip(ClipData.newPlainText("password", result))
        viewModelScope.launch { _oneShotEventFlow.emit(ToastOneShotEvent("已复制结果至粘贴板")) }
    }

    fun onBack() {
        viewModelScope.launch {
            _oneShotEventFlow.emit(FinishOneShotEvent)
        }
    }

    private var loadingJob: Job? = null

    fun sendChatMessage(chatMessage: OpenAiService.ChatMessage) {
        loadingJob?.cancel()
        _uiStateFlow.update { it.copy(loadingType = LoadingType.LOADING) }
        loadingJob = viewModelScope.launch(CoroutineExceptionHandler { _, throwable ->
            Log.d("临时测试", throwable.localizedMessage)
            viewModelScope.launch {
                _oneShotEventFlow.emit(ToastOneShotEvent("网络错误，也许是你请求得太快了！"))
            }
        }) {
            withContext(Dispatchers.IO) {
                val responseBody =
                    repo.sendChatContent(OpenAiService.ChatContent(messages = listOf(chatMessage)))
                val source = responseBody.source()
                val buffer = Buffer()
                var result = ""
                val currentTime = System.currentTimeMillis()
                _uiStateFlow.value.qaBeanList.addAll(
                    listOf(
                        QABean(
                            content = chatMessage.content,
                            isMe = true,
                            time = currentTime
                        ),
                        QABean(
                            content = "",
                            false,
                            time = currentTime + 1
                        ),
                    )
                )
                var lastQaBean = _uiStateFlow.value.qaBeanList.last()

                while (!source.exhausted() && isActive) {
                    source.read(buffer, 8192)
                    val currentResult = buffer.readUtf8().replaceFirst("data: ", "")
                    buffer.clear()
                    if (currentResult == "[DONE]\n\n") {
                        return@withContext
                    }
                    val jsonObject = JSONObject(currentResult)
                    val realCurrentResult =
                        jsonObject.optJSONArray("choices")?.getJSONObject(0)?.optString("text")
                    if (result.isEmpty() && realCurrentResult == "\n") {
                        continue
                    }
                    result += realCurrentResult
                    lastQaBean = lastQaBean.copy(
                        content = result
                    )
                    _uiStateFlow.value.qaBeanList[_uiStateFlow.value.qaBeanList.lastIndex] =
                        lastQaBean
                }
                source.close()
            }
        }.also {
            it.invokeOnCompletion {
                _uiStateFlow.update { uiState ->
                    uiState.copy(loadingType = LoadingType.IDLE)
                }
            }
        }

    }

    fun startNewAnswer(question: OpenAiService.Question) {
        loadingJob?.cancel()
        _uiStateFlow.update { it.copy(loadingType = LoadingType.LOADING) }
        loadingJob =
            viewModelScope.launch(CoroutineExceptionHandler { _, _ ->
                viewModelScope.launch {
                    _oneShotEventFlow.emit(ToastOneShotEvent("网络错误，也许是你请求得太快了！"))
                }
            }) {
                withContext(Dispatchers.IO) {
                    val responseBody = repo.startNewAnswer(question)
                    val source = responseBody.source()
                    val buffer = Buffer()
                    var result = ""
                    val currentTime = System.currentTimeMillis()
                    _uiStateFlow.value.qaBeanList.addAll(
                        listOf(
                            QABean(
                                content = question.prompt,
                                isMe = true,
                                time = currentTime
                            ),
                            QABean(
                                content = "",
                                false,
                                time = currentTime + 1
                            ),
                        )
                    )
                    var lastQaBean = _uiStateFlow.value.qaBeanList.last()

                    while (!source.exhausted() && isActive) {
                        source.read(buffer, 8192)
                        val currentResult = buffer.readUtf8().replaceFirst("data: ", "")
                        buffer.clear()
                        if (currentResult == "[DONE]\n\n") {
                            return@withContext
                        }
                        val jsonObject = JSONObject(currentResult)
                        val realCurrentResult =
                            jsonObject.optJSONArray("choices")?.getJSONObject(0)?.optString("text")
                        if (result.isEmpty() && realCurrentResult == "\n") {
                            continue
                        }
                        result += realCurrentResult
                        lastQaBean = lastQaBean.copy(
                            content = result
                        )
                        _uiStateFlow.value.qaBeanList[_uiStateFlow.value.qaBeanList.lastIndex] =
                            lastQaBean
                    }
                    source.close()
                }
            }.also {
                it.invokeOnCompletion {
                    _uiStateFlow.update { uiState ->
                        uiState.copy(loadingType = LoadingType.IDLE)
                    }
                }
            }
    }

}

interface OneShotEvent

data class ToastOneShotEvent(val content: String) : OneShotEvent

object FinishOneShotEvent : OneShotEvent