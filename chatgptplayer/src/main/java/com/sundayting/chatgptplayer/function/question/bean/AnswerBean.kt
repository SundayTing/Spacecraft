package com.sundayting.chatgptplayer.function.question.bean

import com.google.gson.annotations.SerializedName

data class AnswerBean(
    val error: Error? = null,
    val id: String,
    val model: String,
    val choices: List<Choice> = emptyList(),
)

data class Choice(
    val text: String,
    val index: Int,
    @SerializedName("finish_reason")
    val finishReason: String = "",
)

data class Error(
    val error: String,
)