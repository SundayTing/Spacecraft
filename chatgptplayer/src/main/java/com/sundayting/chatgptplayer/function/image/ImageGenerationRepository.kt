package com.sundayting.chatgptplayer.function.image
import com.sundayting.chatgptplayer.function.question.repo.OpenAiService
import com.sundayting.chatgptplayer.function.service.ReplicateService
import javax.inject.Inject

class ImageGenerationRepository @Inject constructor(
    private val openAiService: OpenAiService,
    private val replicateService: ReplicateService,
) {

    suspend fun fetchOpenAIImage(
        prompt: String,
    ) = openAiService.fetchImage(
        OpenAiService.ImageParam(
            prompt = prompt
        )
    )

    suspend fun predictImage(
        prompt: String,
    ) = replicateService.predictImage(
        ReplicateService.ReplicateImageRequestBody(
            input = ReplicateService.Input(
                prompt = prompt
            )
        )
    )

    suspend fun getPredictedResult(id: String) =
        replicateService.getPredictedResult(id)

}