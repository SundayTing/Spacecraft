package com.sundayting.chatgptplayer.net.module

import com.sundayting.com.network.HaloCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RetrofitModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class OpenAI

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class Replicate

    @Singleton
    @OpenAI
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory,
        callAdapterFactory: CallAdapter.Factory,
        @OpenAI url: String,
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .baseUrl(url)
            .build()
    }

    @Singleton
    @Replicate
    @Provides
    fun provideReplicateRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory,
        callAdapterFactory: CallAdapter.Factory,
        @Replicate url: String,
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(callAdapterFactory)
            .baseUrl(url)
            .build()
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapter.Factory {
        return HaloCallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @OpenAI
    @Provides
    @Singleton
    fun provideOpenAIUrl(): String {
        return "https://api.openai.com/v1/"
    }

    @Replicate
    @Provides
    @Singleton
    fun provideReplicateUrl(): String {
        return "https://api.replicate.com/v1/"
    }

}