package com.sundayting.chatgptplayer.net.module

import com.sundayting.chatgptplayer.net.interceptor.CommonParamsInterceptor
import com.sundayting.com.network.interceptor.AdapterLoggingInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object OkhttpModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class TimeOut

    @Provides
    @Singleton
    fun provideOkhttpClient(
        @TimeOut timeOut: Long,
        //公参拦截器
        commonParamsInterceptor: CommonParamsInterceptor,
        //日志拦截器
        loggingInterceptor: AdapterLoggingInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(commonParamsInterceptor)
            .addNetworkInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideLogInterceptor(): AdapterLoggingInterceptor {
        return AdapterLoggingInterceptor()
    }

    @TimeOut
    @Provides
    @Singleton
    fun provideTimeOutSeconds(): Long {
        return 60 * 1000L
    }

}