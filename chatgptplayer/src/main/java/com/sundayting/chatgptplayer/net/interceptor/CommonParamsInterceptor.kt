package com.sundayting.chatgptplayer.net.interceptor

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

private const val METHOD_GET = "GET"
private const val METHOD_POST = "POST"

@Singleton
class CommonParamsInterceptor @Inject constructor(

) : Interceptor {


    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return try {
            chain.proceed(
                request
                    .newBuilder()
                    .apply {
                        generateCommonParams(request).forEach { entry ->
                            addHeader(entry.key, entry.value)
                        }
                    }
                    .build()
            )
        } catch (e: Exception) {
            //发生异常，原路返回
            chain.proceed(chain.request())
        }
    }

    private fun generateCommonParams(request: Request): Map<String, String> {
        return when (request.url.host) {
            "api.openai.com" -> mapOf(
                "Authorization" to "Bearer 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"
            )

            "api.replicate.com" -> mapOf(
                "Authorization" to "Token db3f407049139b06290959963359f42aadb0b802"
            )

            else -> {
                mapOf()
            }
        }
    }

}