package com.sundayting.chatgptplayer

import android.app.Application
import com.sundayting.chatgptplayer.function.question.bean.AnswerBean
import com.sundayting.com.network.NResult
import com.sundayting.com.network.exception.isServerErrorException
import com.sundayting.com.network.global.ExceptionMsgNativeLangTransformer
import com.sundayting.com.network.global.HaloInitializer
import com.sundayting.com.network.global.NetworkResultTransformer
import com.sundayting.com.network.ktx.isNSuccess
import com.sundayting.com.network.ktx.toNFailure
import dagger.hilt.android.HiltAndroidApp
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@HiltAndroidApp
class ChatGptApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        HaloInitializer.init(
            resultTransformer = object : NetworkResultTransformer {
                override fun <T> transformerBy(nResult: NResult<T>): NResult<T> {
                    //只关注成功的和酷麦实体类的情况
                    if (nResult.isNSuccess() && (nResult.body is AnswerBean)) {
                        val answerBean = nResult.body as AnswerBean
                        if (answerBean.error != null) {
                            return NetErrorException(answerBean.error.error).toNFailure()
                        }

                    }
                    return nResult
                }
            },
            nativeLangTransformer = object : ExceptionMsgNativeLangTransformer {
                override fun rawToNativeMsg(throwable: Throwable): String {
                    return if (throwable.isNetErrorException()) {
                        throwable.msg
                    } else if (throwable.isServerErrorException()) {
                        when (throwable.code) {
                            429 -> {
                                "HTTP状态码错误[429]：请求速度太快了！"
                            }

                            402 -> {
                                "请求速率过快，请等待"
                            }

                            else -> {
                                "HTTP状态码错误[${throwable.code}]：原因未知"
                            }
                        }
                    } else {
                        "网络错误"
                    }
                }

            })
    }

}


/**
 * 后台网络异常
 */
class NetErrorException internal constructor(val msg: String) : Exception()

@OptIn(ExperimentalContracts::class)
fun Throwable.isNetErrorException(): Boolean {
    contract {
        returns(true) implies (this@isNetErrorException is NetErrorException)
    }
    return this is NetErrorException
}