package com.sundayting.chatgptplayer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sundayting.chatgptplayer.playground.NestedScrollList2
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NestedScrollList2()
//            NestedScrollTest()
//            MainScreen(onClickFunction = { function ->
//                when (function) {
//                    Function.TEXT_COMPLETION -> {
//                        startActivity(Intent(this, QuestionActivity::class.java))
//                    }
//
//                    Function.IMAGE_GENERATION -> {
//                        startActivity(Intent(this, ImageGenerationActivity::class.java))
//                    }
//
//                    Function.MORE -> {
//                        Toast.makeText(this, "请期待！", Toast.LENGTH_SHORT).show()
//                    }
//                }
//            })
//            FullHeightBottomSheet(header = { Box(contentAlignment = Alignment.Center) { Text("拉我") } }) {
//                Column{
//                    for (i in 1..40) {
//                        Card(backgroundColor = Color.LightGray) {
//                            Text(
//                                "哈哈哈哈哈哈", modifier = Modifier
//                                    .fillMaxWidth()
//                                    .padding(10.dp)
//                            )
//                        }
//                    }
//                }
//
//            }
        }
    }

}

enum class Function(
    val funName: String,
    val subTitle: String? = null,
    @DrawableRes val drawableId: Int,
) {
    TEXT_COMPLETION(funName = "你问我答", drawableId = R.drawable.ic_answer),
    IMAGE_GENERATION(
        funName = "图片生成",
        subTitle = "现支持2种模型",
        drawableId = R.drawable.ic_image
    ),
    MORE(funName = "更多制作中", drawableId = R.drawable.ic_more)
}

@Composable
fun MainScreen(
    onClickFunction: (Function) -> Unit,
) {
    val items = remember {
        listOf(
            Function.TEXT_COMPLETION,
            Function.IMAGE_GENERATION,
            Function.MORE,
        )
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.White),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(30.dp))
        Text("AI功能百宝箱", fontSize = 40.sp)
        Spacer(modifier = Modifier.size(10.dp))
        Text(
            "作者：小庭  版本：beta v0.1",
            style = TextStyle(color = Color.Gray.copy(alpha = 0.9f), fontSize = 20.sp)
        )
        Spacer(modifier = Modifier.size(20.dp))
        FunctionItemList(items = items, onClickFunction)
    }

}

@Composable
@Preview
fun PreviewFunctionItem() {
    val items = remember {
        listOf(
            Function.TEXT_COMPLETION,
            Function.IMAGE_GENERATION,
            Function.TEXT_COMPLETION,
            Function.IMAGE_GENERATION,
            Function.TEXT_COMPLETION,
            Function.IMAGE_GENERATION,
            Function.TEXT_COMPLETION,
        )
    }
    FunctionItemList(items = items)
}

@Composable
fun FunctionItemList(
    items: List<Function>,
    onClick: (Function) -> Unit = {},
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        modifier = Modifier.fillMaxSize()
    ) {
        items(items) { item ->
            Box(Modifier.aspectRatio(1f), contentAlignment = Alignment.Center) {
                FunctionItem(item, onClick = { onClick(item) })
            }
        }
    }
}

@Composable
fun FunctionItem(
    function: Function,
    onClick: () -> Unit,
) {

    Surface(
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier.padding(10.dp),
        border = BorderStroke(1.dp, Color.Gray.copy(alpha = 0.1f)),
        elevation = 5.dp
    ) {

        Box(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .clickable { onClick() }
                    .padding(10.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    function.funName,
                    fontSize = 25.sp,
                    color = Color.Black,
                    textAlign = TextAlign.Center
                )
                Spacer(modifier = Modifier.size(30.dp))
                Image(
                    painter = painterResource(id = function.drawableId),
                    contentDescription = null,
                    modifier = Modifier.size(50.dp)
                )
            }
            if (function.subTitle != null) {
                Box(
                    modifier = Modifier
                        .clip(RoundedCornerShape(bottomEnd = 10.dp))
                        .background(color = Color.Red.copy(alpha = 0.4f))
                        .padding(5.dp)
                ) {
                    Text(
                        text = function.subTitle,
                        color = Color.White
                    )
                }

            }

        }


    }

}