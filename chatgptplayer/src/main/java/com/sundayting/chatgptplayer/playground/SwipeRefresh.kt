package com.sundayting.chatgptplayer.playground

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.tween
import androidx.compose.foundation.MutatePriority
import androidx.compose.foundation.MutatorMutex
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

@Stable
class SwipeRefreshState {
    private val mutatorMutex = MutatorMutex()
    private val indicatorOffsetAnimatable = Animatable(0.dp, Dp.VectorConverter)
    val indicatorOffset get() = indicatorOffsetAnimatable.value
    private val _indicatorOffsetFlow = MutableStateFlow(0f)
    val indicatorOffsetFlow: Flow<Float> get() = _indicatorOffsetFlow
    val isSwipeInProgress by derivedStateOf { indicatorOffset != 0.dp }
    var isRefreshing: Boolean by mutableStateOf(false)

    fun updateOffsetDelta(value: Float) {
        _indicatorOffsetFlow.value = value
    }

    suspend fun snapToOffset(value: Dp) {
        mutatorMutex.mutate(MutatePriority.UserInput) {
            indicatorOffsetAnimatable.snapTo(value)
        }
    }

    suspend fun animateToOffset(value: Dp) {
        mutatorMutex.mutate {
            indicatorOffsetAnimatable.animateTo(value, tween(1000))
        }
    }
}

@Composable
fun rememberSwipeRefreshState(): SwipeRefreshState {
    return remember { SwipeRefreshState() }
}


@Composable
fun SwipeRefresh() {
    val refreshState = rememberSwipeRefreshState()
    val refreshHeight = remember { 60.dp }
    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                if (source == NestedScrollSource.Drag && available.y < 0) {
                    refreshState.updateOffsetDelta(available.y)
                    return if (refreshState.isRefreshing) Offset(
                        x = 0f,
                        y = available.y
                    ) else Offset.Zero
                }
                return Offset.Zero
            }

            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                if (source == NestedScrollSource.Drag && available.y > 0) {
                    refreshState.updateOffsetDelta(available.y)
                    return Offset(x = 0f, y = available.y)
                }
                return Offset.Zero
            }

            override suspend fun onPreFling(available: Velocity): Velocity {
                if (refreshState.indicatorOffset > refreshHeight / 2) {
                    refreshState.animateToOffset(refreshHeight)
                    refreshState.isRefreshing = true
                } else {
                    refreshState.animateToOffset(0.dp)
                }
                return super.onPreFling(available)
            }

        }
    }
    Box(
        Modifier.nestedScroll(nestedScrollConnection),
        contentAlignment = Alignment.TopCenter
    ) {
        val density = LocalDensity.current
        LaunchedEffect(LocalDensity.current) {
            refreshState.indicatorOffsetFlow.collect {
                val currentOffset = with(density) { refreshState.indicatorOffset + it.toDp() }
                refreshState.snapToOffset(currentOffset.coerceIn(0.dp, refreshHeight))
            }
        }
        LaunchedEffect(refreshState.isRefreshing) {
            if (refreshState.isRefreshing) {
                refreshState.animateToOffset(0.dp)
                refreshState.isRefreshing = false
            }
        }
        Box(Modifier.offset(y = -refreshHeight + refreshState.indicatorOffset)) {
            CircularProgressIndicator()
        }
        Box(Modifier.offset(y = refreshState.indicatorOffset)) {
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                items(100) {
                    Text("拉我")
                }
            }
        }
    }
}
