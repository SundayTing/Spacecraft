package com.sundayting.chatgptplayer.playground

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.rememberSplineBasedDecay
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.DraggableState
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.Text
import androidx.compose.material.rememberSwipeableState
import androidx.compose.material.swipeable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollDispatcher
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@Composable
@Preview(showBackground = true)
fun NestedScrollList2() {
    val strList = remember { (0..100).map { "当前item:$it" } }


    val topContentHeight = remember { 300.dp }
    val density = LocalDensity.current
    val topContentHeightPx = remember(density) {
        with(density) { topContentHeight.toPx() }
    }

    var topContentOffsetY by remember { mutableStateOf(0f) }
    val topContentOffsetYDp = remember(density, topContentOffsetY) {
        with(density) { topContentOffsetY.toDp() }
    }

    val topContentShowDp = remember(topContentOffsetYDp) {
        topContentHeight + topContentOffsetYDp
    }

    val lazyScrollState = rememberLazyListState()

    val connection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                if (delta < 0) {
                    return if (topContentOffsetY == -topContentHeightPx) {
                        Offset.Zero
                    } else {
                        topContentOffsetY =
                            (topContentOffsetY + delta).coerceIn(-topContentHeightPx, 0f)
                        Offset(0f, delta)
                    }
                }
                return Offset.Zero
            }

            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                if (available.y > 0) {
                    return if (topContentOffsetY == 0f) {
                        Offset.Zero
                    } else {
                        topContentOffsetY =
                            (topContentOffsetY + available.y).coerceIn(-topContentHeightPx, 0f)
                        Offset(0f, available.y)
                    }

                }

                return Offset.Zero
            }
        }
    }

    BoxWithConstraints(
        Modifier
            .fillMaxSize()
            .nestedScroll(connection)
    ) {

        val decay = rememberSplineBasedDecay<Float>()
        val draggableState = remember {
            DraggableState {
                topContentOffsetY =
                    (topContentOffsetY + it).coerceIn(-topContentHeightPx, 0f)
            }
        }
        Box(
            Modifier
                .draggable(
                    state = draggableState,
                    orientation = Orientation.Vertical,
                    onDragStopped = { velocity ->
                    })
                .offset(0.dp, topContentOffsetYDp)
                .fillMaxWidth()
                .height(topContentHeight)
                .padding(horizontal = 20.dp)
                .background(remember { Color.Red.copy(alpha = 0.2f) })
        )
        LazyColumn(
            Modifier
                .offset(0.dp, topContentOffsetYDp + topContentHeight)
                .fillMaxWidth()
                .height(maxHeight)
                .clip(RoundedCornerShape(topEnd = 15.dp, topStart = 15.dp))
                .background(remember { Color.Green.copy(alpha = 0.1f) })
                .padding(15.dp),
            state = lazyScrollState
        ) {
            items(strList) {
                Text(text = it)
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun NestedScrollList() {

    val strList = remember { (0..100).map { "当前item:$it" } }


    val topContentHeight = remember { 300.dp }
    val density = LocalDensity.current
    val topContentHeightPx = remember(density) {
        with(density) { topContentHeight.toPx() }
    }

    var topContentOffsetY by remember { mutableStateOf(0f) }
    val topContentOffsetYDp = remember(density, topContentOffsetY) {
        with(density) { topContentOffsetY.toDp() }
    }

    val topContentShowDp = remember(topContentOffsetYDp) {
        topContentHeight + topContentOffsetYDp
    }

    val lazyScrollState = rememberLazyListState()

    val connection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                if (delta < 0) {
                    return if (topContentOffsetY == -topContentHeightPx) {
                        Offset.Zero
                    } else {
                        topContentOffsetY =
                            (topContentOffsetY + delta).coerceIn(-topContentHeightPx, 0f)
                        Offset(0f, delta)
                    }
                }
                return Offset.Zero
            }

            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                if (available.y > 0) {
                    return if (topContentOffsetY == 0f) {
                        Offset.Zero
                    } else {
                        topContentOffsetY =
                            (topContentOffsetY + available.y).coerceIn(-topContentHeightPx, 0f)
                        Offset(0f, available.y)
                    }

                }

                return Offset.Zero
            }
        }
    }

    Box(
        Modifier
            .fillMaxSize()
            .nestedScroll(connection)
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .offset(0.dp, topContentOffsetYDp)
                .height(topContentHeight)
                .background(Color.Red)

        )
        LazyColumn(
            Modifier
                .fillMaxSize(),
            contentPadding = PaddingValues(top = topContentShowDp),
            state = lazyScrollState
        ) {
            items(strList) {
                Text(text = it)
            }
        }
    }

}

@Preview
@Composable
fun NestedScrollTest() {
    Box(
        modifier = Modifier
            .background(Color.LightGray)
            .verticalScroll(rememberScrollState())
            .padding(32.dp)
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(6.dp)
        ) {
            repeat(10) {
                Box(
                    Modifier
                        .height(128.dp)
                        .verticalScroll(rememberScrollState())
                ) {
                    Text(
                        "滑动这里",
                        modifier = Modifier
                            .background(brush = remember {
                                Brush.verticalGradient(listOf(Color.Gray, Color.Red))
                            })
                            .padding(24.dp)
                            .height(350.dp)
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun NestedScrollTest2() {
    val lazyListState = rememberLazyListState()
    val headerHeight = remember { 60.dp }
    val headerHeightPx = with(LocalDensity.current) { headerHeight.toPx() }
    var headerOffsetPx by remember { mutableStateOf(0f) }
    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                //向下滑
                if (available.y > 0) {
                    //当list滑到尽头的时候，才去让header出来
                    if (lazyListState.firstVisibleItemIndex <= 1) {
                        val delta = available.y
                        headerOffsetPx =
                            (headerOffsetPx + delta).coerceIn(-headerHeightPx, 0f)
                    }
                } else {
                    val delta = available.y
                    headerOffsetPx =
                        (headerOffsetPx + delta.toInt()).coerceIn(-headerHeightPx, 0f)
                }
                return Offset.Zero
            }
        }
    }
    val nestedScrollDispatcher = remember { NestedScrollDispatcher() }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .nestedScroll(nestedScrollConnection, nestedScrollDispatcher)
    ) {
        LazyColumn(
            state = lazyListState,
            contentPadding = PaddingValues(top = headerHeight)
        ) {
            items(100) { index ->
                Text(
                    "I'm item $index", modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                )
            }
        }
        Box(
            Modifier
                .height(headerHeight)
                .offset { IntOffset(0, headerOffsetPx.toInt()) }
                .fillMaxWidth()
                .background(color = Color.Red)
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
@Preview
fun SwipeableWidget() {
    val swipeableState = rememberSwipeableState(initialValue = 0)
    val sizePx = with(LocalDensity.current) { 48.dp.toPx() }
    Box(
        modifier = Modifier
            .width(96.dp)
            .height(48.dp)
            .swipeable(
                state = swipeableState,
                anchors = remember {
                    mapOf(
                        0f to 0,
                        sizePx to 1
                    )
                },
                orientation = Orientation.Horizontal,
                thresholds = { _, _ ->
                    FractionalThreshold(0.5f)
                },
            )
            .background(Color.LightGray)
    ) {
        Box(
            Modifier
                .offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
                .size(48.dp)
                .background(Color.DarkGray)
        )
    }
}

enum class States {
    EXPANDED,
    COLLAPSED
}

@ExperimentalMaterialApi
@Composable
fun FullHeightBottomSheet(
    header: @Composable () -> Unit,
    body: @Composable () -> Unit
) {
    val swipeableState = rememberSwipeableState(initialValue = States.EXPANDED)
    val scrollState = rememberScrollState()

    BoxWithConstraints {
        val constraintsScope = this
        val maxHeight = with(LocalDensity.current) {
            constraintsScope.maxHeight.toPx()
        }

        val connection = remember {
            object : NestedScrollConnection {

                override fun onPreScroll(
                    available: Offset,
                    source: NestedScrollSource
                ): Offset {
                    val delta = available.y
                    return if (delta < 0) {
                        swipeableState.performDrag(delta).toOffset()
                    } else {
                        Offset.Zero
                    }
                }

                override fun onPostScroll(
                    consumed: Offset,
                    available: Offset,
                    source: NestedScrollSource
                ): Offset {
                    val delta = available.y
                    return swipeableState.performDrag(delta).toOffset()
                }

                override suspend fun onPreFling(available: Velocity): Velocity {
                    return if (available.y < 0 && scrollState.value == 0) {
                        swipeableState.performFling(available.y)
                        available
                    } else {
                        Velocity.Zero
                    }
                }

                override suspend fun onPostFling(
                    consumed: Velocity,
                    available: Velocity
                ): Velocity {
                    swipeableState.performFling(velocity = available.y)
                    return super.onPostFling(consumed, available)
                }

                private fun Float.toOffset() = Offset(0f, this)
            }
        }

        Box(
            Modifier
                .swipeable(
                    state = swipeableState,
                    orientation = Orientation.Vertical,
                    anchors = mapOf(
                        0f to States.EXPANDED,
                        maxHeight to States.COLLAPSED,
                    )
                )
                .nestedScroll(connection)
                .offset {
                    IntOffset(
                        0,
                        swipeableState.offset.value.roundToInt()
                    )
                }
        ) {
            Column(
                Modifier
                    .fillMaxHeight()
                    .background(Color.White)
            ) {
                header()
                Box(
                    Modifier
                        .fillMaxWidth()
                        .verticalScroll(scrollState)
                ) {
                    body()
                }
            }
        }
    }
}

@Composable
@Preview
fun TestSlider() {
    var value by remember { mutableStateOf(0f) }
    Column {
        Text(text = "${value}")
        CustomSeekbar(
            onProgressChanged = { value = it },
            modifier = Modifier
                .fillMaxWidth()
                .height(30.dp)
        )
    }

}

@Composable
fun CustomSeekbar(
    modifier: Modifier,
    onProgressChanged: (progress: Float) -> Unit
) {
    // 当前进度，范围0-1之间， 初始为0
    var progress by remember { mutableStateOf(0f) }
    // bar是否被按下
    var barPressed by remember { mutableStateOf(false) }
    // 锚点的半径, 根据barPressed的状态'平滑'地改变自身的大小
    val radius by animateDpAsState(if (barPressed) 9.dp else 7.5.dp)
    val radiusPx = with(LocalDensity.current) { radius.toPx() }
    Canvas(
        modifier = modifier
            .pointerInput(Unit) {
                detectDragGestures( // 响应滑动事件
                    onDragStart = { barPressed = true },
                    onDragCancel = { barPressed = false },
                    onDragEnd = {
                        // 滑动结束时， 恢复锚点大小，并回调onProgressChanged函数
                        barPressed = false
                        onProgressChanged(progress)
                    },
                    onDrag = { change, dragAmount ->
                        // 滑动过程中， 实时刷新progress的值(注意左右边界的问题)，
                        // 此值一旦改变， 整个Seekbar就会重组(刷新)
                        progress = if (change.position.x < 0) {
                            0f
                        } else if (change.position.x > size.width) {
                            1f
                        } else {
                            (change.position.x / this.size.width)
                        }
                    })
            }
            .pointerInput(Unit) {
                // 响应点击事件， 直接跳到该进度处
                detectTapGestures(onTap = {
                    progress = (it.x / size.width)
                    onProgressChanged(progress)
                    barPressed = false
                })
            },
        onDraw = {
            // 锚点
            drawCircle(
                color = Color.Blue,
                radius = radiusPx,
                center = Offset(size.width * progress, size.height / 2)
            )
        })
}

@Composable
@Preview(showBackground = true)
fun TestNestScroll() {
    var offsetY by remember { mutableStateOf(0f) }
    val dispatcher = remember { NestedScrollDispatcher() }
    val connection = remember {
        object : NestedScrollConnection {
            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                offsetY += available.y
                return available
            }
        }
    }
    Column(
        modifier = Modifier
            .offset { IntOffset(0, offsetY.roundToInt()) }
            .draggable(rememberDraggableState {
                val consumed = dispatcher.dispatchPreScroll(Offset(0f, it), NestedScrollSource.Drag)
                offsetY += it - consumed.y
                dispatcher.dispatchPostScroll(
                    Offset(0f, it),
                    Offset(0f, 0f),
                    NestedScrollSource.Drag
                )
            }, Orientation.Vertical)
            .nestedScroll(connection, dispatcher)
    ) {
        for (i in 1..10) {
            Text("第 $i 项")
        }
        LazyColumn(
            modifier = Modifier
                .height(50.dp)
                .background(color = Color.Red)
        ) {
            items(10) {
                Text("第 ${it + 1} 项")
            }
        }
    }
}