package com.sundayting.chatgptplayer.playground

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp

@Composable
fun SwipeRefreshTest() {

//    var toolBarHeight by remember { mutableStateOf(60.dp) }
    val density = LocalDensity.current
    val toolBarHeight = 60.dp
    val toolBarHeightPx = remember(density) {
        with(density) { toolBarHeight.toPx() }
    }

    var toolBarOffsetPx by remember { mutableStateOf(0f) }
    val toolBarOffset by remember(density) {
        derivedStateOf { with(density) { toolBarOffsetPx.toDp() } }
    }
    val lazyScrollState = rememberLazyListState()
    LaunchedEffect(key1 = Unit) {
        snapshotFlow { lazyScrollState.firstVisibleItemIndex }.collect {
            Log.d("临时测试", it.toString())
        }
    }

    val nestedScrollConnection = remember {
        object : NestedScrollConnection {
            override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                val delta = available.y
                if (delta < 0 && source == NestedScrollSource.Drag) {
                    if ((-toolBarOffset) < toolBarHeight) {
                        toolBarOffsetPx = (toolBarOffsetPx + delta).coerceIn(-toolBarHeightPx, 0f)
                        return Offset(0f, delta)
                    }
                }
                return Offset.Zero
            }

            override fun onPostScroll(
                consumed: Offset,
                available: Offset,
                source: NestedScrollSource
            ): Offset {
                val delta = available.y
                if (delta > 0) {
                    if (lazyScrollState.firstVisibleItemIndex == 0 && lazyScrollState.firstVisibleItemScrollOffset == 0) {
                        if (toolBarOffset < 0.dp) {
                            toolBarOffsetPx =
                                (toolBarOffsetPx + delta).coerceIn(-toolBarHeightPx, 0f)
                            return Offset(0f, delta)
                        }
                    }
                }
                return Offset.Zero
            }
        }
    }

    Box(
        Modifier
            .fillMaxSize()
            .nestedScroll(nestedScrollConnection)
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .height(toolBarHeight)
                .offset { IntOffset(0, toolBarOffsetPx.toInt()) }
                .background(remember { Color.Green.copy(alpha = 0.2f) })
        )
        LazyColumn(
            Modifier.fillMaxSize(),
            state = lazyScrollState,
            contentPadding = PaddingValues(top = toolBarHeight + toolBarOffset)
        ) {
            items(count = 1000, contentType = { 1 }) {
                Text("哈哈哈哈:$it", modifier = Modifier.padding(20.dp), color = Color.Black)
            }
        }
    }

}