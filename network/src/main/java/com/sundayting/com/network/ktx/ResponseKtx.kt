package com.sundayting.com.network.ktx

import com.sundayting.com.network.NResult
import com.sundayting.com.network.exception.FailureReason
import com.sundayting.com.network.exception.ServerErrorException
import com.sundayting.com.network.global.HaloInitializer
import retrofit2.Response

//fun ResponseBody.downloadAndCount(
//    dest: File,
//    onDownloadListener: DownloadListener
//) {
//    val contentLength = contentLength()
//    var totalRead = 0L
//    try {
//        if (!dest.exists()) {
//            dest.createNewFile()
//        }
//        dest.sink().buffer().use { bufferedSink ->
//            bufferedSink.writeAll(object : ForwardingSource(source()) {
//                override fun read(sink: Buffer, byteCount: Long): Long {
//                    val bytesRead = super.read(sink, byteCount)
//                    if (bytesRead != -1L) {
//                        totalRead += bytesRead
//                        onDownloadListener.onProgressChange(totalRead, contentLength)
//                    }
//                    return bytesRead
//                }
//            })
//        }
//    } catch (e: Exception) {
//        onDownloadListener.onStop(
//            totalBytes = contentLength, downloadBytes = totalRead,
//            exception = e
//        )
//    } finally {
//        onDownloadListener.onFinish(totalBytes = contentLength, downloadBytes = totalRead)
//    }
//}
//
//interface DownloadListener {
//    fun onProgressChange(downloadBytes: Long, totalBytes: Long)
//    fun onFinish(downloadBytes: Long, totalBytes: Long) {}
//    fun onStop(downloadBytes: Long, totalBytes: Long, exception: Exception?) {}
//}


/**
 * 将[Response]转译成[NResult]
 *
 * 转换规则如下：
 *
 * 如果网络请求成功，即HTTP协议码处于200-300之间，则返回[NResult.NSuccess]
 *
 * 如果网络请求失败或者发生其他异常，则返回[NResult.NFailure]
 */
fun <T> Response<T>.toNetResult(): NResult<T> = try {
    if (isSuccessful) {
        toNSuccess()
    } else {
        toNFailure()
    }
} catch (t: Throwable) {
    t.toNFailure()
}

/**
 * 将[Response]转译为[NResult.NSuccess]
 */
fun <T> Response<T>.toNSuccess(): NResult.NSuccess<T> {
    assert(isSuccessful) { "严禁将失败的http请求转成成功结果！" }
    return NResult.NSuccess(this)
}

/**
 * 将[Response]转译为[NResult.NFailure]，这种情况只发生于HTTP协议错误的情况下
 *
 * 其中，[NResult.NFailure]的异常会通过[FailureReason]包裹
 */
fun Response<*>.toNFailure(): NResult.NFailure {
    assert(!isSuccessful) { "严禁将成功的http请求转成失败结果！" }
    val serverException = ServerErrorException(code(), errorBody())
    return NResult.NFailure(
        FailureReason(
            HaloInitializer.instance.nativeLangTransformer.rawToNativeMsg(serverException),
            serverException
        )
    )
}

fun Throwable.toNFailure(): NResult.NFailure {
    return NResult.NFailure(
        FailureReason(
            HaloInitializer.instance.nativeLangTransformer.rawToNativeMsg(this),
            this
        )
    )
}


