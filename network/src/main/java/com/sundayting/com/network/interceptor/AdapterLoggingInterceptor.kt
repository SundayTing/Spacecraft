package com.sundayting.com.network.interceptor

import android.util.Log
import com.sundayting.com.network.ktx.getMethodAnnotation
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.Streaming

class AdapterLoggingInterceptor : Interceptor {

    private val bodyInterceptor = HttpLoggingInterceptor(
        logger = {
            Log.d("网络请求日志", it)
        }
    ).apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val headerInterceptor = HttpLoggingInterceptor(
        logger = {
            Log.d("网络请求日志", it)
        }
    ).apply {
        level = HttpLoggingInterceptor.Level.HEADERS
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        return if (chain.request().getMethodAnnotation(Streaming::class.java) != null) {
            headerInterceptor.intercept(chain)
        } else {
            bodyInterceptor.intercept(chain)
        }
    }


}