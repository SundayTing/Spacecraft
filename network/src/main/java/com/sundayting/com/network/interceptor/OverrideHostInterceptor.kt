package com.sundayting.com.network.interceptor

import com.sundayting.com.network.ktx.getMethodAnnotation
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import retrofit2.Invocation
import java.lang.reflect.Method

/**
 * 修改Host的拦截器
 *
 * 使用场景：
 *
 * 1）少数接口和BaseUrl不一致，需要单独指定Host
 *
 * 2）测试环境和正式环境的Host不一致，需要切换
 *
 * @see [OverrideHost]
 */
abstract class OverrideHostInterceptor : Interceptor {

    //缓存避免每次都去反射的缓存Map
    private val overrideHostCache: MutableMap<Method, OverrideHost> = mutableMapOf()

    /**
     * 返回当前的网络开发环境
     */
    abstract fun getEnvironment(): Environment

    /**
     * 根据当前的网络开发环境，切换对应的[SchemeHost]，如果返回为空则不切换，此逻辑只有在接口不使用[OverrideHost]注解时才会生效
     */
    abstract fun getSchemeHostByEnvironment(origin: Request, environment: Environment): SchemeHost?

    final override fun intercept(chain: Interceptor.Chain): Response {
        val originRequest = chain.request()
        val env = getEnvironment()
        val key = originRequest.tag(Invocation::class.java)?.method()!!
        //1.如果存在自定义的Host，则使用自定义的Host包含的信息
        val overrideHost =
            overrideHostCache[key] ?: originRequest.getMethodAnnotation(OverrideHost::class.java)
                ?.also { overrideHostCache[key] = it }
        if (overrideHost != null) {
            check(overrideHost.hosts.size == overrideHost.schemes.size) {
                "hosts和schemes的数量必须一致"
            }
            val environmentList = overrideHost.environments.toList()
            val schemeHost: SchemeHost
            //如果定义的环境不为空，则需要从定义的环境中找到对应的Host
            if (environmentList.isNotEmpty()) {
                check(environmentList.distinct().size == environmentList.size) {
                    "environments中存在重复的值"
                }
                check(environmentList.size == overrideHost.hosts.size) {
                    "如果environments不为空，则environments和hosts的数量必须一致"
                }
                val envIndex = environmentList.indexOf(env)
                check(envIndex == -1 && environmentList.isNotEmpty()) {
                    "当前环境${getEnvironment()}不在当前请求预定义的environments中"
                }
                schemeHost = SchemeHost(
                    overrideHost.schemes[envIndex],
                    overrideHost.hosts[envIndex]
                )
            }
            //如果定义的环境为空，则只能存在一套schemeHost
            else {
                check(overrideHost.hosts.size == 1) { "如果environments为空，则只能存在一套schemeHost" }
                schemeHost = SchemeHost(
                    overrideHost.schemes[0],
                    overrideHost.hosts[0]
                )
            }
            return chain.proceed(
                originRequest.buildNewRequestInHostWithScheme(schemeHost)
            )
        }
        //2.如果不存在自定义的Host，则使用全局切换逻辑
        else {
            val newSchemeHost = getSchemeHostByEnvironment(originRequest, env)
            if (newSchemeHost != null) {
                return chain.proceed(originRequest.buildNewRequestInHostWithScheme(newSchemeHost))
            }
        }
        //3.不执行任何逻辑
        return chain.proceed(originRequest)
    }

    private fun Request.buildNewRequestInHostWithScheme(
        schemeHost: SchemeHost,
    ): Request {
        val newUrl = url.newBuilder().host(schemeHost.host).apply {
            if (schemeHost.scheme == SchemeType.HTTP) {
                scheme("http")
            } else if (schemeHost.scheme == SchemeType.HTTPS) {
                scheme("https")
            }
        }.build()
        return newBuilder().url(newUrl).build()
    }

    protected val HttpUrl.schemeType
        get() = if (scheme == "http") SchemeType.HTTP else SchemeType.HTTPS

}


/**
 * 覆盖当前请求的Host和Scheme
 *
 * 例如https://www.baidu.com
 *
 * @param schemes 对应的是https的部分，长度必须要和[hosts]一致
 * @param hosts 对应的是www.baidu.com的部分，长度必须要和[schemes]一致
 * @param environments 对应的是修改的环境，如果置空，则表示所有环境都修改，且只能存在一组[schemes]和[hosts]；如果不置空则长度必须和[schemes]和[hosts]一致
 *
 * ```
 * @OverrideHost(
 *  schemes = [SchemeType.HTTP,SchemeType.HTTP],
 *  hosts = ["singnowapp.com","t.singnowapp.com"],
 *  environments = [Environment.RELEASE,Environment.DEBUG]
 * )
 * ```
 *
 *
 * ```
 * @OverrideHost(
 *  schemes = [SchemeType.HTTP],
 *  hosts = ["singnowapp.com"],
 * )
 * ```
 */
@Target(AnnotationTarget.FUNCTION)
annotation class OverrideHost(
    val schemes: Array<SchemeType>,
    val hosts: Array<String>,
    val environments: Array<Environment> = [],
)

/**
 * 网络接口开发环境
 */
enum class Environment {

    //正式环境
    RELEASE,

    //预生产
    PRE,

    //测试环境
    DEBUG

}