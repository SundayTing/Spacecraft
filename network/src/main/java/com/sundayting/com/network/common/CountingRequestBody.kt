package com.sundayting.com.network.common

import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import okhttp3.RequestBody
import okio.*

class CountingRequestBody internal constructor(
    private val delegate: RequestBody,
) : RequestBody() {

    data class Progress(
        val hasWrittenLen: Long,
        val totalLen: Long,
    )

    private val _progressFlow = MutableStateFlow<Progress?>(null)
    val progressFlow = _progressFlow.asStateFlow()
    val progressLiveData = _progressFlow.asLiveData()

    @Throws(IOException::class)
    override fun contentLength() = delegate.contentLength()

    override fun contentType() = delegate.contentType()

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        val bufferedSink = CountingSink(sink).buffer()
        delegate.writeTo(bufferedSink)
        bufferedSink.flush()
    }

    internal inner class CountingSink(
        delegate: Sink
    ) : ForwardingSink(delegate) {

        //当前写入的字节数
        private var bytesWritten = 0L

        override fun write(source: Buffer, byteCount: Long) {
            super.write(source, byteCount)
            bytesWritten += byteCount
            _progressFlow.update {
                it?.copy(hasWrittenLen = bytesWritten)
                    ?: Progress(
                        hasWrittenLen = bytesWritten,
                        totalLen = contentLength(),
                    )
            }
        }
    }
}