package com.sundayting.com.network.global

import com.sundayting.com.network.NResult


/**
 * 网络结果转换器，将网络请求结果按照自定义的规则转成任意结果
 *
 * 例如校验请求成功的结果中的某些字段后，确认为后台的报错信息，则将成功的结果转换为错误结果
 */
interface NetworkResultTransformer {

    /**
     * 转换网络请求
     */
    fun <T> transformerBy(nResult: NResult<T>): NResult<T> {
        return nResult
    }

    //默认实现
    object Default : NetworkResultTransformer

}