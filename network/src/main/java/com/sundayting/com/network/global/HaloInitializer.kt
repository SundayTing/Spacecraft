package com.sundayting.com.network.global

import com.sundayting.com.network.global.HaloInitializer.Companion.init
import com.sundayting.com.network.global.NetworkResultTransformer.Default


/**
 * 初始化工具，使用Halo之前调用[init]进行初始化
 */
class HaloInitializer private constructor(
    val resultTransformer: NetworkResultTransformer,
    val nativeLangTransformer: ExceptionMsgNativeLangTransformer
) {

    companion object {

        private val DEFAULT = HaloInitializer(
            resultTransformer = Default,
            nativeLangTransformer = ExceptionMsgNativeLangTransformer.Default
        )

        private var realInstance: HaloInitializer? = null
        val instance: HaloInitializer
            get() = realInstance ?: DEFAULT

        @JvmStatic
        fun init(
            resultTransformer: NetworkResultTransformer = Default,
            nativeLangTransformer: ExceptionMsgNativeLangTransformer = ExceptionMsgNativeLangTransformer.Default,
        ) {
            check(realInstance == null) { "HaloInitializer已经被初始化了" }
            realInstance = HaloInitializer(
                resultTransformer = resultTransformer,
                nativeLangTransformer = nativeLangTransformer
            )
        }
    }


}