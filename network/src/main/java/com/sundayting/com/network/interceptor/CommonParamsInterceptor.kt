package com.sundayting.com.network.interceptor

import android.util.Log
import com.sundayting.com.network.ktx.writeToString
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONObject


private const val METHOD_GET = "GET"
private const val METHOD_POST = "POST"

/**
 * 公参拦截器，用于增加公参
 */
abstract class CommonParamsInterceptor : Interceptor {

    companion object {
        private val TAG = CommonParamsInterceptor::class.simpleName!!
        private val TYPE_JSON: MediaType = "application/json; charset=utf-8".toMediaType()
    }

    /**
     * 构建公参
     *
     * *[request]当前请求
     */
    abstract fun generateCommonParams(
        request: Request,
    ): Map<String, String>

    final override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        //Get请求添加参数的方式就是构建新的Url
        if (request.method == METHOD_GET) {
            val newUrl = request.url.newBuilder().also { urlBuilder ->
                generateCommonParams(request).forEach { entry ->
                    urlBuilder.addQueryParameter(entry.key, entry.value)
                }
            }.build()
            return chain.proceed(request.newBuilder().url(newUrl).build())
        }
        //Post请求添加参数的方式是重新构建新的requestBody，此处按照requestBody是Json的类型来处理
        else if (request.method == METHOD_POST) {
            val body = request.body
            if (body != null && body !is MultipartBody) {
                return try {
                    chain.proceed(
                        chain.request().newBuilder().post(addParamsToRequestByPost(request)).build()
                    )
                } catch (e: Exception) {
                    //如果是测试环境，直接抛出异常让开发人员检查
//                    assert(false) { "添加公参失败！原因：${e.localizedMessage}" }
                    Log.e(
                        TAG,
                        "添加公参失败，请求已按无公参形式请求,错误原因：${e.localizedMessage.orEmpty()}\n请求：${request}"
                    )
                    //发生异常，原路返回
                    chain.proceed(chain.request())
                }
            }
        }
        return chain.proceed(chain.request())
    }

    private fun addParamsToRequestByPost(request: Request): RequestBody {
        val requestBodyJson = JSONObject(request.body!!.writeToString())
        generateCommonParams(request).forEach { entry ->
            requestBodyJson.putOpt(entry.key, entry.value)
        }
        return requestBodyJson.toString().toRequestBody(TYPE_JSON)
    }


}