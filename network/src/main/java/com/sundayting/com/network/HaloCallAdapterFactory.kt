package com.sundayting.com.network

import com.sundayting.com.network.internal.HaloResponseCallDelegate
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class HaloCallAdapterFactory private constructor() : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): KuMaiCallAdapter? = when (getRawType(returnType)) {
        Call::class.java -> {
            val callType = getParameterUpperBound(0, returnType as ParameterizedType)
            when (getRawType(callType)) {
                NResult::class.java -> {
                    val resultType = getParameterUpperBound(0, callType as ParameterizedType)
                    KuMaiCallAdapter(resultType)
                }
                else -> null
            }
        }
        else -> null
    }

    companion object {

        fun create(): HaloCallAdapterFactory = HaloCallAdapterFactory()

    }

    class KuMaiCallAdapter constructor(
        private val resultType: Type,
    ) : CallAdapter<Type, Call<NResult<Type>>> {
        override fun responseType() = resultType

        override fun adapt(call: Call<Type>): Call<NResult<Type>> =
            HaloResponseCallDelegate(call)

    }

}