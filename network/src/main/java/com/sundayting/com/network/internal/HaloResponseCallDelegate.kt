package com.sundayting.com.network.internal

import com.sundayting.com.network.NResult
import com.sundayting.com.network.global.HaloInitializer
import com.sundayting.com.network.ktx.toNFailure
import com.sundayting.com.network.ktx.toNetResult
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * retrofit的[Call]代理类，将网络请求统一返回[NResult]
 */
internal class HaloResponseCallDelegate<T>(
    private val proxyCall: Call<T>,
) : Call<NResult<T>> {

    override fun enqueue(callback: Callback<NResult<T>>) =
        proxyCall.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                callback.onResponse(
                    this@HaloResponseCallDelegate,
                    Response.success(
                        HaloInitializer.instance.resultTransformer.transformerBy(
                            response.toNetResult()
                        )
                    )
                )
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                callback.onResponse(
                    this@HaloResponseCallDelegate,
                    Response.success(t.toNFailure())
                )
            }

        })

    override fun isExecuted(): Boolean = proxyCall.isExecuted

    override fun cancel() = proxyCall.cancel()

    override fun isCanceled(): Boolean = proxyCall.isCanceled

    override fun request(): Request = proxyCall.request()

    override fun timeout(): Timeout = proxyCall.timeout()

    override fun clone(): Call<NResult<T>> =
        HaloResponseCallDelegate(proxyCall.clone())

    override fun execute(): Response<NResult<T>> = throw NotImplementedError()


}