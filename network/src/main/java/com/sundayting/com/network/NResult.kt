package com.sundayting.com.network

import com.sundayting.com.network.NResult.NFailure
import com.sundayting.com.network.NResult.NSuccess
import com.sundayting.com.network.exception.FailureReason
import okhttp3.Headers
import retrofit2.Response

/**
 * 网络请求结果
 *
 * 有以下子类：
 *
 * [NSuccess] 网络请求成功
 *
 * [NFailure] 网络请求失败
 *
 */
sealed class NResult<out T> private constructor() {

    /**
     * 网络请求成功
     */
    class NSuccess<T> internal constructor(private val response: Response<T>) : NResult<T>() {

        init {
            check(response.body() != null) { "不要把body为空的response传入${NSuccess::class.simpleName}" }
            check(response.isSuccessful) { "不要把失败的response传入${NSuccess::class.simpleName}中" }
        }

        override fun toString(): String =
            "[${NSuccess::class.simpleName}](body=${body},headers=${headers},code=${httpCode})"

        val body by lazy { response.body()!! }
        val httpCode by lazy { response.code() }
        val httpMessage: String by lazy { response.message() }
        val rawResponse: okhttp3.Response by lazy { response.raw() }
        val headers: Headers by lazy { response.headers() }
    }

    /**
     * 网络请求失败
     */
    class NFailure internal constructor(val failureReason: FailureReason) : NResult<Nothing>() {
        override fun toString(): String =
            "[${NFailure::class.simpleName}](message=${failureReason.message})"
    }

}



