package com.sundayting.com.network.interceptor

data class SchemeHost(
    val scheme: SchemeType,
    val host: String,
)

enum class SchemeType {
    HTTP,
    HTTPS,
}