package com.sundayting.funnyjoke


import android.util.Base64
import org.junit.Test
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private fun String.decrypt(): String {
        val secKey: SecretKey =
            SecretKeySpec(Base64.decode("cretinzp**273846".toByteArray(), Base64.DEFAULT), "AES")
        // 获取 AES 密码器
        val cipher = Cipher.getInstance("AES")
        // 初始化密码器（解密模型）
        cipher.init(Cipher.DECRYPT_MODE, secKey)
        // 解密数据, 返回明文
        return cipher.doFinal(this.toByteArray()).toString(Charset.forName("UTF-8"))
    }

    @Test
    fun test_encrypt() {
        val tmp =
            "ftp://ZQb79GrjmFK1UI4BoacGKeLcxFIp7WGXlVyKPxD8hQUYTrdXQx31WNsMNGGjBZr/Wkf2Xg8QP/NgMQJMuczDP4Xv9epHFkQ4O+ubbyj/AgzsIqdYOV3t8URHenBUPS/SIBlF8Z8ZuUM47fabNanjFoSRo9NAe7Dk9tK+IFeczsO24gB74ZzMzqf1UWstVraB/kWjImbaNECwVHqvuNoghd2XsvuVqaRz/f7NVNovLkoXk9cnQVZetXRvatmHdr/c"
        val result = tmp.replace("ftp://", "").decrypt()
        print(result)
    }
}