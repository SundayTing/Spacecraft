package com.sundayting.funnyjoke.repo

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GlobalRepository @Inject constructor(
    private val globalDataStore: GlobalDataStore,
) {

    fun initDeviceUid() = globalDataStore.initDeviceUid()

    val deviceUid by lazy {
        globalDataStore.deviceUid()
    }

}