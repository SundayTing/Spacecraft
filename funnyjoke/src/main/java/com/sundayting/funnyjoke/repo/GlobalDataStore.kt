package com.sundayting.funnyjoke.repo

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.sundayting.funnyjoke.common.datastore.dataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

/**
 * 全局sp仓库
 */
@Singleton
class GlobalDataStore @Inject constructor(
    @ApplicationContext val context: Context,
) {

    companion object {
        private val DEVICE_UID = stringPreferencesKey("设备唯一id")
    }

    fun initDeviceUid() = runBlocking {
        val uid = context.dataStore.data.map { it[DEVICE_UID] }.first()
        if (uid == null) {
            context.dataStore.edit { it[DEVICE_UID] = UUID.randomUUID().toString() }
        }
    }

    fun deviceUid() = runBlocking {
        context.dataStore.data.map { it[DEVICE_UID] }.first() ?: error("uid未初始化")
    }

}