package com.sundayting.funnyjoke.function.register.bean

import androidx.annotation.IntDef


data class LoginResultBean(
    val token: String = "",
    //登录类型
    val type: String = "",
)

const val LOGIN_SUCCESS = 1
const val REGISTER_SUCCESS = 2

@Retention(AnnotationRetention.SOURCE)
@IntDef(LOGIN_SUCCESS, REGISTER_SUCCESS)
annotation class LoginType

@LoginType
fun LoginResultBean.loginType(): Int {
    return if (type == "login_success") {
        LOGIN_SUCCESS
    } else {
        REGISTER_SUCCESS
    }
}

data class UserInfoBean(
    //头像
    val avatar: String = "",
    //生日
    val birthday: String = "",
    //邀请码
    val inviteCode: String? = "",
    //被邀请码
    val invitedCode: String? = "",
    //昵称
    val nickname: String = "",
    //性别
    val sex: String = "",
    //签名
    val signature: String = "",
    //用户id
    val userId: Int = 0,
    //用户手机（打码过）
    val userPhone: String = "",
)

