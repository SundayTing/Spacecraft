package com.sundayting.funnyjoke.function.home.tab.homepage

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Divider
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import com.sundayting.funnyjoke.R
import com.sundayting.funnyjoke.function.home.tab.homepage.common.JokeListState
import com.sundayting.funnyjoke.function.home.tab.homepage.jokelistpage.JokeListPage
import com.sundayting.funnyjoke.theme.FunnyJokeTheme
import kotlinx.coroutines.launch

@Composable
fun HomePage(
    funnyPicState: JokeListState,
    recommendedState: JokeListState,
    latestState: JokeListState,
    articleState: JokeListState,
) {

    val pagerState = rememberPagerState()

    Column(modifier = Modifier.fillMaxSize()) {
        TabHeader(pagerState)
        HorizontalPager(
            modifier = Modifier.fillMaxWidth(),
            count = 4,
            state = pagerState
        ) { index ->
            when (index) {
                0 -> JokeListPage(modifier = Modifier.fillMaxSize(), state = recommendedState)

                1 -> JokeListPage(modifier = Modifier.fillMaxSize(), state = latestState)

                2 -> JokeListPage(modifier = Modifier.fillMaxSize(), state = articleState)

                3 -> JokeListPage(modifier = Modifier.fillMaxSize(), state = funnyPicState)
            }
        }
    }


}

@Composable
private fun TabHeader(
    pagerState: PagerState,
) {
    val pageText = remember {
        listOf(
            "推荐", "新鲜", "纯文", "趣图"
        )
    }
    Column {
        Row(
            modifier = Modifier.background(Color.White),
            verticalAlignment = Alignment.CenterVertically
        ) {
            TabRow(
                modifier = Modifier.weight(6f),
                selectedTabIndex = pagerState.currentPage,
                backgroundColor = Color.Transparent,
                divider = {},
                indicator = { tabPositions ->
                    TabRowDefaults.Indicator(
                        Modifier.tabIndicatorOffset(tabPositions[pagerState.currentPage]),
                        color = FunnyJokeTheme.colors.primary
                    )
                }
            ) {
                pageText.forEachIndexed { index, title ->
                    val scope = rememberCoroutineScope()
                    Tab(
                        text = {
                            Text(title, fontSize = 20.sp)
                        },
                        selectedContentColor = FunnyJokeTheme.colors.primary,
                        unselectedContentColor = Color.Black,
                        selected = pagerState.currentPage == index,
                        onClick = { scope.launch { pagerState.animateScrollToPage(index) } },
                    )
                }
            }

            Image(
                modifier = Modifier
                    .weight(1f)
                    .padding(10.dp)
                    .size(25.dp),
                painter = painterResource(id = R.drawable.ic_search),
                contentDescription = null
            )
        }
        Divider(modifier = Modifier.fillMaxWidth(), color = Color.Gray.copy(alpha = 0.5f))
    }


}