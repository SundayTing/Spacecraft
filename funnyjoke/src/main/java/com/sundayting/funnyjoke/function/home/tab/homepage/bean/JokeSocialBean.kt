package com.sundayting.funnyjoke.function.home.tab.homepage.bean

data class JokeSocialBean(
    val commentNum: Int = 0,//评论数
    val disLikeNum: Int = 0,//踩数量
    val likeNum: Int = 0,//点赞数
    val shareNum: Int = 0,//分享数
    val isAttention: Boolean = false,//是否关注
    val isLike: Boolean = false,//是否喜欢
    val isUnlike: Boolean = false,//是否踩
)

data class JokeContentBean(
    val addTime: String = "",
    val hot: Boolean = false,
    val type: Int = 0,
    val showAddress: String = "",
    val userId: Int = 0,
    val content: String = "",
    val videoSize: String = "",
    val latitudeLongitude: String = "",
    val videoUrl: String = "",
    val imageUrl: String = "",
    val imageSize: String = "",
    val videoTime: Int = 0,
    val thumbUrl: String = "",
    val auditMsg: String = "",
    val jokesId: Int = 0,
)

data class JokeTotalBean(
    val info: JokeSocialBean,
    val joke: JokeContentBean,
    val user: JokeUser,
)

data class JokeUser(
    val userId: Int = 0,
    val nickName: String = "",
    val signature: String = "",
    val avatar: String = "",
)

