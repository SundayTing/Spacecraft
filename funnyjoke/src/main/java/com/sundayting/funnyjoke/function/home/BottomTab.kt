package com.sundayting.funnyjoke.function.home

import androidx.annotation.DrawableRes
import androidx.annotation.IntRange
import androidx.compose.foundation.Image
import androidx.compose.foundation.Indication
import androidx.compose.foundation.IndicationInstance
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.InteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.drawscope.ContentDrawScope
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.sundayting.funnyjoke.R
import com.sundayting.funnyjoke.extension.textDp
import com.sundayting.funnyjoke.theme.FunnyJokeTheme


object NoIndication : Indication {
    private object NoIndicationInstance : IndicationInstance {
        override fun ContentDrawScope.drawIndication() {
            drawContent()
        }
    }

    @Composable
    override fun rememberUpdatedInstance(interactionSource: InteractionSource): IndicationInstance {
        return NoIndicationInstance
    }
}

@Composable
fun FunnyJokeBottomTab(
    @IntRange(from = 0, to = 3) selectedIndex: Int = 0,
    onAddClick: () -> Unit = {},
    onTabSelected: (Int) -> Unit = {},
) {

    val tabBeanListStart = remember {
        mutableStateListOf(
            BottomTabBean("首页", R.drawable.icon_home),
            BottomTabBean("划一划", R.drawable.icon_video),
        )
    }

    val tabBeanListEnd = remember {
        mutableStateListOf(
            BottomTabBean("消息", R.drawable.icon_message),
            BottomTabBean("我的", R.drawable.icon_mine),
        )
    }

    CompositionLocalProvider(
        LocalIndication provides NoIndication
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = FunnyJokeTheme.colors.cardBackground)
                .padding(horizontal = FunnyJokeTheme.dimensions.paddingLarge),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            tabBeanListStart.forEachIndexed { index, bean ->
                BottomTabItem(
                    modifier = Modifier.clickable { onTabSelected(index) },
                    bean = bean,
                    isSelected = index == selectedIndex
                )
            }

            Image(
                modifier = Modifier
                    .size(60.dp)
                    .padding(bottom = 10.dp, top = 5.dp)
                    .clickable { onAddClick() },
                painter = painterResource(id = R.drawable.icon_add), contentDescription = null,
                colorFilter = ColorFilter.tint(color = FunnyJokeTheme.colors.primary)
            )

            tabBeanListEnd.forEachIndexed { index, bean ->
                BottomTabItem(
                    modifier = Modifier.clickable { onTabSelected(index + 2) },
                    bean = bean,
                    isSelected = index == selectedIndex - 2
                )
            }
        }
    }

}

@Preview(showBackground = true)
@Composable
fun PreviewFunnyJokeBottomTab() {
    var selectedIndex by remember {
        mutableStateOf(0)
    }
    FunnyJokeBottomTab(selectedIndex) {
        selectedIndex = it
    }
}

data class BottomTabBean(
    val name: String,
    @DrawableRes val resId: Int,
)

@Composable
fun BottomTabItem(
    modifier: Modifier = Modifier,
    bean: BottomTabBean,
    isSelected: Boolean,
) {
    Column(
        modifier = modifier.padding(vertical = FunnyJokeTheme.dimensions.paddingSmall),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = bean.resId),
            contentDescription = null,
            colorFilter = if (isSelected) ColorFilter.tint(color = FunnyJokeTheme.colors.primary) else null,
            modifier = Modifier.size(25.dp)
        )
        Text(
            text = bean.name,
            color = if (isSelected) FunnyJokeTheme.colors.primary
            else FunnyJokeTheme.colors.textSecondary,
            fontSize = 12.textDp
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewBottomTabItem() {
    BottomTabItem(
        bean = BottomTabBean("首页", R.drawable.icon_home),
        isSelected = true
    )
}