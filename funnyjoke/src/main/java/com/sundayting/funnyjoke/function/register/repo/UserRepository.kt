package com.sundayting.funnyjoke.function.register.repo

import com.sundayting.funnyjoke.common.db.FunnyJokeDB
import com.sundayting.funnyjoke.function.common.FunnyJokeService
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.mapLatest
import retrofit2.http.Field
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val funnyJokeService: FunnyJokeService,
    private val funnyJokeDB: FunnyJokeDB,
    private val userDataStore: UserDataStore,
) {

    val userDetailFlow = userDataStore.idFlow.mapLatest { id ->
        if (id == UserDataStore.EMPTY_ID) {
            null
        } else {
            funnyJokeDB.userDao().queryUserById(id)
        }
    }.flowOn(Dispatchers.IO)

    suspend fun updateCurrentUserDetail(userDetailBean: UserDetailBean?) {
        //登录
        if (userDetailBean != null) {
            funnyJokeDB.userDao().insertUsers(userDetailBean)
            userDataStore.updateUserId(userDetailBean.id)
        }
        //退出登录
        else {
            userDataStore.logOut()
        }
    }

    suspend fun updateToken(token: String) = userDataStore.updateToken(token)


    suspend fun getCode(
        @Field("phone") phone: String,
    ) = funnyJokeService.getCode(phone)

    suspend fun loginByCode(
        @Field("phone") phone: String,
        @Field("code") code: String,
    ) = funnyJokeService.loginByCode(phone, code)

    suspend fun getUserInfo() = funnyJokeService.getUserInfo()

}