package com.sundayting.funnyjoke.function.home

import androidx.annotation.IntRange
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeScreenViewModel @Inject constructor() : ViewModel() {

    var homeState by mutableStateOf(UiState())
        private set

    data class UiState(
        val selectedIndex: Int = 0,
    )

    fun selectTab(@IntRange(from = 0, to = 3) index: Int) {
        homeState = homeState.copy(selectedIndex = index)
    }

}