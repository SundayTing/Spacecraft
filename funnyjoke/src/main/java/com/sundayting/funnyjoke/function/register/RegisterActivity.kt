package com.sundayting.funnyjoke.function.register

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.core.view.WindowCompat
import androidx.lifecycle.lifecycleScope
import com.sundayting.funnyjoke.common.event.FinishEvent
import com.sundayting.funnyjoke.theme.FunnyJokeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterActivity : ComponentActivity() {

    private val viewModel by viewModels<RegisterViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            FunnyJokeTheme {
                RegisterScreen(onBack = {
                    finish()
                })
            }
        }

        lifecycleScope.launch {
            viewModel.oneShowEventFlow.filterIsInstance<FinishEvent>().collect { finish() }
        }
    }
}