package com.sundayting.funnyjoke.function.home.tab.minepage

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import com.sundayting.funnyjoke.function.register.repo.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MineViewModel @Inject constructor(
    userRepository: UserRepository,
) : ViewModel() {

    var mineState by mutableStateOf(UiState())
        private set

    data class UiState(
        val userDetailBean: UserDetailBean? = null,
    )

    init {
        viewModelScope.launch {
            userRepository.userDetailFlow.collectLatest {
                mineState = mineState.copy(
                    userDetailBean = it
                )
            }
        }
    }

}