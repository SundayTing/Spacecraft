package com.sundayting.funnyjoke.function.common

import com.sundayting.com.network.NResult
import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeTotalBean
import com.sundayting.funnyjoke.function.register.bean.LoginResultBean
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import com.sundayting.funnyjoke.network.JokeBean
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import javax.inject.Singleton

interface FunnyJokeService {

    /**
     * 获取验证码
     * @param phone 手机号
     */
    @FormUrlEncoded
    @POST("user/login/get_code")
    suspend fun getCode(
        @Field("phone") phone: String,
    ): NResult<JokeBean<Any>>

    /**
     * 验证码登录
     * @param code 验证码
     * @param phone 手机
     */
    @FormUrlEncoded
    @POST("user/login/code")
    suspend fun loginByCode(
        @Field("phone") phone: String,
        @Field("code") code: String,
    ): NResult<JokeBean<LoginResultBean>>

    /**
     * 获取用户信息
     */
    @POST("user/info")
    suspend fun getUserInfo(): NResult<JokeBean<UserDetailBean>>

    /**
     * 获取首页推荐列表
     */
    @POST("home/recommend")
    suspend fun fetchRecommendJokeList(): NResult<JokeBean<List<JokeTotalBean>>>

    /**
     * 获取首页趣图列表
     */
    @POST("home/pic")
    suspend fun fetchPicJokeList(): NResult<JokeBean<List<JokeTotalBean>>>

    /**
     * 获取首页最新列表
     */
    @POST("home/latest")
    suspend fun fetchLatestJokeList(): NResult<JokeBean<List<JokeTotalBean>>>

    /**
     * 获取首页纯文列表
     */
    @POST("home/text")
    suspend fun fetchArticleJokeList(): NResult<JokeBean<List<JokeTotalBean>>>

}

@InstallIn(SingletonComponent::class)
@Module
object ServiceModule {

    @Provides
    @Singleton
    fun provideFunnyJokeService(
        retrofit: Retrofit,
    ): FunnyJokeService {
        return retrofit.create(FunnyJokeService::class.java)
    }

}

