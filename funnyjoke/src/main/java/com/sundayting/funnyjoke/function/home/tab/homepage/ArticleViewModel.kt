package com.sundayting.funnyjoke.function.home.tab.homepage

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.com.network.ktx.isNSuccess
import com.sundayting.funnyjoke.function.home.tab.homepage.common.JokeListState
import com.sundayting.funnyjoke.function.home.tab.homepage.repo.JokeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.collections.immutable.toImmutableList
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleViewModel @Inject constructor(
    private val jokeRepo: JokeRepository,
) : ViewModel() {

    var state by mutableStateOf(JokeListState(onLoad = { refresh ->
        if (refresh) {
            refresh()
        } else {
            loadMore()
        }
    }))
        private set


    init {
        refresh()
    }

    private var loadingJob: Job? = null

    private fun refresh() = loadingData(true)

    private fun loadMore() = loadingData(false)

    private fun loadingData(isRefreshing: Boolean) {
        loadingJob?.cancel()
        state = state.copy(refreshing = false, loadingMore = false)
        loadingJob = viewModelScope.launch {
            state = state.copy(refreshing = isRefreshing, loadingMore = !isRefreshing)
            val result = jokeRepo.fetchArticleJokeList()
            if (result.isNSuccess()) {
                val dataList = result.body.data.orEmpty()
                state = if (isRefreshing) {
                    state.copy(
                        jokeList = dataList.toImmutableList()
                    )
                } else {
                    state.copy(
                        jokeList = (state.jokeList + dataList).toImmutableList()
                    )
                }
            }
        }.also {
            it.invokeOnCompletion {
                state = state.copy(refreshing = false, loadingMore = false)
            }
        }
    }


}