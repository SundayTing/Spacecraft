package com.sundayting.funnyjoke.function.home.tab.messagepage

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun MessagePage() {

    Box(modifier = Modifier.fillMaxSize()) {
        Text("消息", modifier = Modifier.align(Alignment.Center))
    }

}