package com.sundayting.funnyjoke.function.register.repo

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.sundayting.funnyjoke.common.datastore.dataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataStore @Inject constructor(
    @ApplicationContext private val context: Context,
) {

    private val scope = MainScope()

    companion object {

        const val EMPTY_ID = -1
        private val CURRENT_TOKEN_KEY = stringPreferencesKey("当前用户token")
        private val CURRENT_USER_ID = intPreferencesKey("当前用户ID")

    }

    suspend fun updateToken(token: String) {
        context.dataStore.edit { it[CURRENT_TOKEN_KEY] = token }
    }

    suspend fun updateUserId(id: Int) {
        context.dataStore.edit { it[CURRENT_USER_ID] = id }
    }

    suspend fun logOut() {
        context.dataStore.edit {
            it[CURRENT_TOKEN_KEY] = ""
            it[CURRENT_USER_ID] = EMPTY_ID
        }
    }

    val tokenFlow = context.dataStore.data.map { it[CURRENT_TOKEN_KEY] ?: "" }
        .stateIn(scope, SharingStarted.Eagerly, "")

    val idFlow = context.dataStore.data.map { it[CURRENT_USER_ID] ?: EMPTY_ID }
        .stateIn(scope, SharingStarted.Eagerly, EMPTY_ID)

}