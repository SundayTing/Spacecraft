package com.sundayting.funnyjoke.function.home

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.sundayting.funnyjoke.common.SafeArea
import com.sundayting.funnyjoke.function.home.tab.homepage.ArticleViewModel
import com.sundayting.funnyjoke.function.home.tab.homepage.FunnyPicViewModel
import com.sundayting.funnyjoke.function.home.tab.homepage.HomePage
import com.sundayting.funnyjoke.function.home.tab.homepage.LatestViewModel
import com.sundayting.funnyjoke.function.home.tab.homepage.RecommendViewModel
import com.sundayting.funnyjoke.function.home.tab.messagepage.MessagePage
import com.sundayting.funnyjoke.function.home.tab.minepage.MinePage
import com.sundayting.funnyjoke.function.home.tab.minepage.MineViewModel
import com.sundayting.funnyjoke.function.home.tab.videopage.VideoPage
import com.sundayting.funnyjoke.function.register.RegisterActivity
import com.sundayting.funnyjoke.theme.FunnyJokeTheme

@Composable
fun HomeScreen(
    viewModel: HomeScreenViewModel = viewModel(),
    funnyPicViewModel: FunnyPicViewModel = viewModel(),
    recommendViewModel: RecommendViewModel = viewModel(),
    articleViewModel: ArticleViewModel = viewModel(),
    latestViewModel: LatestViewModel = viewModel(),
    mineViewModel: MineViewModel = viewModel(),
) {
    val state = viewModel.homeState
    val mineState = mineViewModel.mineState
    val context = LocalContext.current
    SafeArea {
        Column {
            Box(
                modifier = Modifier
                    .weight(weight = 1f)
                    .background(color = FunnyJokeTheme.colors.background)
            ) {
                when (state.selectedIndex) {
                    0 -> HomePage(
                        funnyPicState = funnyPicViewModel.state,
                        recommendedState = recommendViewModel.state,
                        latestState = latestViewModel.state,
                        articleState = articleViewModel.state
                    )

                    1 -> VideoPage()
                    2 -> MessagePage()
                    3 -> MinePage(
                        userDetailBean = mineState.userDetailBean,
                        clickHeaderRow = {
                            if (mineState.userDetailBean == null) {
                                context.startActivity(Intent(context, RegisterActivity::class.java))
                            }
                        }
                    )

                    else -> {
                        throw IllegalArgumentException("未知的tab")
                    }
                }
            }

            FunnyJokeBottomTab(
                selectedIndex = state.selectedIndex,
                onTabSelected = { index ->
                    //如果没有登录，则跳转到注册
                    if (index == 2 && mineState.userDetailBean == null) {
                        context.startActivity(Intent(context, RegisterActivity::class.java))
                    } else {
                        viewModel.selectTab(index)
                    }
                },
                onAddClick = {
                    Toast.makeText(context, "add", Toast.LENGTH_SHORT).show()
                }
            )
            Spacer(modifier = Modifier.navigationBarsPadding())
        }
    }
}
