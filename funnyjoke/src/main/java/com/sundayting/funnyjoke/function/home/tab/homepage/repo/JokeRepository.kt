package com.sundayting.funnyjoke.function.home.tab.homepage.repo

import com.sundayting.funnyjoke.function.common.FunnyJokeService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class JokeRepository @Inject constructor(
    private val funnyJokeService: FunnyJokeService,
) {


    suspend fun fetchRecommendJokeList() = funnyJokeService.fetchRecommendJokeList()

    suspend fun fetchPicJokeList() = funnyJokeService.fetchPicJokeList()

    suspend fun fetchLatestJokeList() = funnyJokeService.fetchLatestJokeList()

    suspend fun fetchArticleJokeList() = funnyJokeService.fetchArticleJokeList()

}