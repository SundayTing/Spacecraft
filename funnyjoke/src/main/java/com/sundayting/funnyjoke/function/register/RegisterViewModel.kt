package com.sundayting.funnyjoke.function.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.com.network.ktx.isNSuccess
import com.sundayting.funnyjoke.common.event.FinishEvent
import com.sundayting.funnyjoke.common.event.OneShotEvent
import com.sundayting.funnyjoke.common.event.ToastEvent
import com.sundayting.funnyjoke.function.register.repo.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repo: UserRepository,
    private val userRepo: UserRepository,
) : ViewModel() {

    private val _stateFlow = MutableStateFlow(RegisterState())
    val stateFlow = _stateFlow.asStateFlow()

    val phoneCompleteFlow = stateFlow.map { it.inputAccount.length == 11 }

    private val _oneShotEventFlow = MutableSharedFlow<OneShotEvent>()
    val oneShowEventFlow = _oneShotEventFlow.asSharedFlow()

    private val canLoginFlow = _stateFlow.map { it.inputAccount }
        .combine(_stateFlow.map { it.inputPassword }) { account, password ->
            account.length == 11 && password.isNotEmpty()
        }.stateIn(viewModelScope, SharingStarted.Eagerly, false)

    init {
        viewModelScope.launch {
            canLoginFlow.collect { canLogin ->
                _stateFlow.update { it.copy(canLogin = canLogin) }
            }
        }
    }

    data class RegisterState(
        val loading: Boolean = false,//加载框
        val passwordLogin: Boolean = false,//是否密码登陆
        val inputAccount: String = "",//账号
        val inputPassword: String = "",//密码
        val canLogin: Boolean = false,//是否可登录
        val verifyCountDown: Int = 0,//验证码倒计时
    )

    private var countDownJob: Job? = null

    /**
     * 获取验证码
     */
    fun getVerifyCode() {
        viewModelScope.launch {
            _stateFlow.update { it.copy(loading = true) }
            val result = repo.getCode(_stateFlow.value.inputAccount)
            if (result.isNSuccess()) {
                startVerifyCountDown()
                _oneShotEventFlow.emit(ToastEvent("发送验证码成功，请在小程序中查看"))
            } else {
                _oneShotEventFlow.emit(ToastEvent(result.failureReason.message))
            }

        }.invokeOnCompletion {
            _stateFlow.update { it.copy(loading = false) }
        }
    }

    fun loginByCode() {
        viewModelScope.launch {
            _stateFlow.update { it.copy(loading = true) }
            val loginResult =
                repo.loginByCode(stateFlow.value.inputAccount, stateFlow.value.inputPassword)
            if (loginResult.isNSuccess()) {
                userRepo.updateToken(loginResult.body.data?.token.orEmpty())
                val getUserInfoResult = repo.getUserInfo()
                if (getUserInfoResult.isNSuccess()) {
                    _oneShotEventFlow.emit(ToastEvent("登录成功！"))
                    _oneShotEventFlow.emit(FinishEvent)
                    val data = getUserInfoResult.body.data
                    if (data != null) {
                        userRepo.updateCurrentUserDetail(data.copy(id = data.user.userId))
                    } else {
                        userRepo.updateCurrentUserDetail(null)
                    }
                } else {
                    _oneShotEventFlow.emit(ToastEvent(getUserInfoResult.failureReason.message))
                }
            } else {
                _oneShotEventFlow.emit(ToastEvent(loginResult.failureReason.message))
            }
        }.invokeOnCompletion {
            _stateFlow.update { it.copy(loading = false) }
        }
    }

    /**
     * 开始验证码倒计时
     */
    private fun startVerifyCountDown() {
        countDownJob?.cancel()
        viewModelScope.launch {
            _stateFlow.update { it.copy(verifyCountDown = 60) }
            while (isActive && _stateFlow.value.verifyCountDown > 0) {
                delay(1000L)
                _stateFlow.update {
                    it.copy(verifyCountDown = it.verifyCountDown - 1)
                }
            }
        }
    }

    fun togglePasswordLogin() {
        _stateFlow.update {
            it.copy(passwordLogin = !_stateFlow.value.passwordLogin)
        }
    }

    fun changeInputText(text: String) {
        if (text.length > 11) {
            return
        }
        _stateFlow.update {
            it.copy(inputAccount = text.filter { char -> !char.isWhitespace() })
        }
    }

    fun changeInputPassword(text: String) {
        _stateFlow.update {
            it.copy(inputPassword = text.filter { char -> !char.isWhitespace() })
        }
    }

}