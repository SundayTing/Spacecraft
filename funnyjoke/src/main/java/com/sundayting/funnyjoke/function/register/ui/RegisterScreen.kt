package com.sundayting.funnyjoke.function.register

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.sundayting.funnyjoke.R
import com.sundayting.funnyjoke.common.SafeArea
import com.sundayting.funnyjoke.common.composeable.LoadingDialog
import com.sundayting.funnyjoke.common.event.ToastEvent
import com.sundayting.funnyjoke.extension.collectAsEffect
import com.sundayting.funnyjoke.extension.toast
import com.sundayting.funnyjoke.theme.FunnyJokeTheme

@Composable
@Preview(showBackground = true)
fun PreviewRegisterScreen() {
    RegisterScreen()
}

@Composable
fun RegisterScreen(
    onBack: () -> Unit = {},
    viewModel: RegisterViewModel = viewModel(),
) {
    val state by viewModel.stateFlow.collectAsStateWithLifecycle(initialValue = RegisterViewModel.RegisterState())
    val context = LocalContext.current
    viewModel.oneShowEventFlow.collectAsEffect { effect ->
        when (effect) {
            is ToastEvent -> context.toast(effect.content)
        }
    }
    val phoneComplete by viewModel.phoneCompleteFlow.collectAsStateWithLifecycle(initialValue = false)

    val focusManager = LocalFocusManager.current
    if (state.loading) {
        LoadingDialog()
    }
    SafeArea(
        modifier = Modifier
            .fillMaxSize()
            .background(color = FunnyJokeTheme.colors.background)
            .padding(
                top = FunnyJokeTheme.dimensions.paddingMedium,
                start = FunnyJokeTheme.dimensions.paddingLarge,
                end = FunnyJokeTheme.dimensions.paddingLarge,
                bottom = FunnyJokeTheme.dimensions.paddingLarge
            )
    ) {
        Image(
            painter = painterResource(id = R.drawable.icon_close),
            contentDescription = null,
            modifier = Modifier
                .padding(top = FunnyJokeTheme.dimensions.paddingSmall)
                .size(25.dp)
                .clickable { onBack() }
        )

        Text(
            modifier = Modifier.padding(top = FunnyJokeTheme.dimensions.paddingLarge),
            text = if (state.passwordLogin) "密码登录" else "验证码登录",
            style = FunnyJokeTheme.typography.h1.copy(fontWeight = FontWeight.Bold),
        )

        val normalStyle = FunnyJokeTheme.typography.h2
        val labelStyle = remember(normalStyle) {
            normalStyle.copy(
                normalStyle.color.copy(0.4f)
            )
        }

        Box(
            modifier = Modifier
                .padding(top = FunnyJokeTheme.dimensions.paddingLarge)
                .background(
                    color = Color.Gray.copy(0.1f),
                    shape = RoundedCornerShape(50)
                )
        ) {
            TextField(
                modifier = Modifier.fillMaxWidth(),
                textStyle = normalStyle,
                value = state.inputAccount,
                onValueChange = {
                    viewModel.changeInputText(it)
                },
                singleLine = true,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    unfocusedBorderColor = Color.Transparent,
                    focusedLabelColor = Color.Transparent,
                    cursorColor = FunnyJokeTheme.colors.primary,
                    focusedBorderColor = Color.Transparent
                ),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Next
                ),
                keyboardActions = KeyboardActions(
                    onNext = {
                        focusManager.moveFocus(FocusDirection.Down)
                    }
                )
            )
            if (state.inputAccount.isEmpty()) {
                Text(
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(start = 18.dp),
                    text = "请输入手机号",
                    style = labelStyle
                )
            }
        }

        Box(
            modifier = Modifier
                .padding(top = FunnyJokeTheme.dimensions.paddingLarge)
                .background(
                    color = Color.Gray.copy(0.1f),
                    shape = RoundedCornerShape(50)
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(modifier = Modifier.weight(2f)) {
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        textStyle = normalStyle,
                        value = state.inputPassword,
                        onValueChange = {
                            viewModel.changeInputPassword(it)
                        },
                        singleLine = true,
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            unfocusedBorderColor = Color.Transparent,
                            focusedLabelColor = Color.Transparent,
                            cursorColor = FunnyJokeTheme.colors.primary,
                            focusedBorderColor = Color.Transparent
                        ),
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Password,
                            imeAction = ImeAction.Done
                        ),
                        keyboardActions = KeyboardActions(
                            onDone = { viewModel.loginByCode() }
                        ),
                        visualTransformation = PasswordVisualTransformation()
                    )
                    if (state.inputPassword.isEmpty()) {
                        Text(
                            modifier = Modifier
                                .align(Alignment.CenterStart)
                                .padding(start = 18.dp),
                            text = "请输入验证码",
                            style = labelStyle
                        )
                    }
                }
                Row(
                    modifier = Modifier.weight(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Spacer(
                        modifier = Modifier
                            .height(15.dp)
                            .width(1.dp)
                            .background(color = Color.Gray.copy(alpha = 0.2f))
                    )
                    val h4 = FunnyJokeTheme.typography.h4
                    val primaryColor = FunnyJokeTheme.colors.primary
                    val disableStyle = remember(h4) {
                        h4.copy(color = Color.Gray.copy(0.4f))
                    }
                    val enableStyle = remember(h4, primaryColor) {
                        h4.copy(color = primaryColor)
                    }
                    Text(
                        if (state.verifyCountDown > 0) "${state.verifyCountDown}秒" else "获取验证码",
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentWidth(align = Alignment.CenterHorizontally)
                            .clickable {
                                if (phoneComplete && state.verifyCountDown == 0) {
                                    viewModel.getVerifyCode()
                                }
                            },
                        style = if (phoneComplete && state.verifyCountDown == 0) {
                            enableStyle
                        } else {
                            disableStyle
                        }
                    )
                }
            }

        }

        OutlinedButton(
            onClick = { viewModel.loginByCode() },
            enabled = state.canLogin,
            shape = RoundedCornerShape(50),
            border = BorderStroke(width = 0.dp, color = Color.Transparent),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = FunnyJokeTheme.dimensions.paddingLarge),
            colors = ButtonDefaults.outlinedButtonColors(
                backgroundColor = if (state.canLogin) FunnyJokeTheme.colors.primary else Color.Gray.copy(
                    alpha = 0.2f
                )
            ),
            contentPadding = PaddingValues(vertical = 15.dp)
        ) {
            Text("登陆", style = FunnyJokeTheme.typography.h3.copy(color = Color.White))
        }

        Row(modifier = Modifier.padding(top = FunnyJokeTheme.dimensions.paddingLarge)) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.End)
                    .clickable { },
                style = FunnyJokeTheme.typography.h4.copy(color = FunnyJokeTheme.colors.primary),
                text = "遇到问题？",
            )
        }
    }
}