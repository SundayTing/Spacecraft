package com.sundayting.funnyjoke.function.home.tab.videopage

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun VideoPage() {
    Box(modifier = Modifier.fillMaxSize()) {
        Text("视频", modifier = Modifier.align(Alignment.Center))
    }
}