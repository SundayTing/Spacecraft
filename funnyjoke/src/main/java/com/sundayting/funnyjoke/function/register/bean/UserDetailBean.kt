package com.sundayting.funnyjoke.function.register.bean

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

data class UserSocialBean(
    //关注数量
    val attentionNum: Int = 0,
    //经验
    val experienceNum: Int = 0,
    //粉丝数量
    val fansNum: Int = 0,
    //喜欢数量
    val likeNum: Int = 0,
)

@Entity
data class UserDetailBean(
    @PrimaryKey
    val id: Int,
    @Embedded
    val info: UserSocialBean = UserSocialBean(),
    @Embedded
    val user: UserInfoBean = UserInfoBean(),
)