package com.sundayting.funnyjoke.function.home.tab.minepage

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.sundayting.funnyjoke.R
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import com.sundayting.funnyjoke.function.register.bean.UserInfoBean
import com.sundayting.funnyjoke.theme.FunnyJokeTheme

@Composable
@Preview
fun PreviewMinePage() {
    MinePage()
}

@Composable
fun MinePage(
    userDetailBean: UserDetailBean? = null,
    clickHeaderRow: () -> Unit = {},
) {

    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = FunnyJokeTheme.colors.background)
            .padding(FunnyJokeTheme.dimensions.paddingLarge)
            .verticalScroll(scrollState)
    ) {
        HeaderRow(userDetailBean?.user, clickHeaderRow)
        Row(
            horizontalArrangement = Arrangement.spacedBy(FunnyJokeTheme.dimensions.paddingLarge),
            modifier = Modifier.padding(
                vertical = FunnyJokeTheme.dimensions.paddingLarge,
                horizontal = FunnyJokeTheme.dimensions.paddingMedium
            )
        ) {
            NumDataItem(name = "关注", value = userDetailBean?.info?.attentionNum?.toString())
            NumDataItem(name = "粉丝", value = userDetailBean?.info?.fansNum?.toString())
            NumDataItem(name = "乐豆", value = userDetailBean?.info?.experienceNum?.toString())
        }
    }

}

@Composable
private fun NumDataItem(
    name: String,
    value: String?,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = value ?: "-", style = FunnyJokeTheme.typography.h2)
        Text(
            text = name,
            style = FunnyJokeTheme.typography.h4.copy(color = Color.Gray.copy(alpha = 0.7f))
        )
    }
}

@Composable
private fun HeaderRow(
    userInfoBean: UserInfoBean?,
    clickHeaderRow: () -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = FunnyJokeTheme.dimensions.paddingMedium)
            .clickable { clickHeaderRow() },
        verticalAlignment = Alignment.CenterVertically
    ) {

        val painter =
            if (userInfoBean != null) {
                val avatarDecrypted = remember(userInfoBean.avatar) {
                    userInfoBean.avatar
                }
                rememberAsyncImagePainter(model = avatarDecrypted)
            } else {
                painterResource(id = R.drawable.ic_default_head)
            }
        Image(
            painter = painter,
            contentDescription = null,
            modifier = Modifier.size(50.dp)
        )


        Spacer(modifier = Modifier.width(20.dp))

        Column(verticalArrangement = Arrangement.SpaceBetween) {
            Text(
                text = userInfoBean?.nickname ?: "登录/注册",
                style = FunnyJokeTheme.typography.h1
            )
            Text(
                text = userInfoBean?.signature ?: "快来开始你的创作吧~",
                style = FunnyJokeTheme.typography.h4.copy(
                    color = Color.Black.copy(alpha = 0.7f)
                )
            )
        }

        Spacer(
            modifier = Modifier
                .weight(1f, false)
                .fillMaxWidth()
        )
        Image(
            painter = painterResource(id = R.drawable.ic_arrow_right),
            contentDescription = null,
            modifier = Modifier.size(20.dp),
            colorFilter = ColorFilter.tint(color = Color.Black.copy(alpha = 0.7f))
        )
    }
}