package com.sundayting.funnyjoke.function.home.tab.homepage.jokelistpage

import androidx.annotation.DrawableRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import coil.compose.SubcomposeAsyncImage
import coil.compose.SubcomposeAsyncImageContent
import coil.compose.rememberAsyncImagePainter
import coil.decode.GifDecoder
import coil.request.ImageRequest
import com.sundayting.funnyjoke.R
import com.sundayting.funnyjoke.common.composeable.VerticalGrid
import com.sundayting.funnyjoke.common.composeable.VideoPlayer
import com.sundayting.funnyjoke.common.funnyJokeDecrypt
import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeContentBean
import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeSocialBean
import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeTotalBean
import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeUser
import com.sundayting.funnyjoke.function.home.tab.homepage.common.JokeListState
import com.sundayting.funnyjoke.theme.FunnyJokeTheme
import kotlinx.collections.immutable.persistentListOf

@Composable
@Preview
fun PreviewJokeListPage() {
    val list = remember {
        persistentListOf(
            JokeTotalBean(
                info = JokeSocialBean(),
                joke = JokeContentBean(
                    content = "哈哈哈哈哈哈哈哈哈哈\n哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈", jokesId = 1
                ),
                user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
            ),
            JokeTotalBean(
                info = JokeSocialBean(),
                joke = JokeContentBean(
                    content = "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈", jokesId = 2
                ),
                user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
            ),
            JokeTotalBean(
                info = JokeSocialBean(),
                joke = JokeContentBean(
                    content = "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈", jokesId = 3
                ),
                user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
            ),
            JokeTotalBean(
                info = JokeSocialBean(),
                joke = JokeContentBean(
                    content = "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈", jokesId = 4
                ),
                user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
            ), JokeTotalBean(
                info = JokeSocialBean(),
                joke = JokeContentBean(
                    content = "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈\n" +
                            "哈哈哈哈哈哈哈哈哈哈", jokesId = 5
                ),
                user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
            )
        )
    }
    JokeListPage(
        modifier = Modifier
            .fillMaxSize()
            .background(FunnyJokeTheme.colors.background),
        state = JokeListState(jokeList = list)
    )
}

@Composable
fun JokeListPage(modifier: Modifier = Modifier, state: JokeListState) {

    val lazyScrollState = rememberLazyListState()
    val pullRefreshState = rememberPullRefreshState(refreshing = state.refreshing, onRefresh = {
        state.onLoad(true)
    })
    val isAtBottom by remember {
        derivedStateOf {
            val layoutInfo = lazyScrollState.layoutInfo
            val visibleItemsInfo = layoutInfo.visibleItemsInfo
            if (layoutInfo.totalItemsCount == 0) {
                false
            } else {
                val lastVisibleItem = visibleItemsInfo.last()
                val viewportHeight = layoutInfo.viewportEndOffset + layoutInfo.viewportStartOffset

                (lastVisibleItem.index + 1 == layoutInfo.totalItemsCount &&
                        lastVisibleItem.offset + lastVisibleItem.size <= viewportHeight)
            }
        }
    }

    DisposableEffect(key1 = isAtBottom) {
        if (isAtBottom && !state.loadingMore) {
            state.onLoad(false)
        }
        onDispose { }
    }

    Box(
        modifier = Modifier
            .pullRefresh(pullRefreshState, enabled = !state.loadingMore)
            .clipToBounds()
    ) {
        LazyColumn(
            modifier = modifier,
            state = lazyScrollState,
        ) {
            items(
                items = state.jokeList,
                key = { it.joke.jokesId },
                contentType = { 1 }
            ) { bean: JokeTotalBean ->
                JokeItem(jokeBean = bean)
            }
        }
        PullRefreshIndicator(
            state.refreshing,
            pullRefreshState,
            Modifier.align(Alignment.TopCenter),
            contentColor = FunnyJokeTheme.colors.primary
        )
    }
}

@Composable
@Preview
private fun PreviewJokeItem() {
    JokeItem(
        JokeTotalBean(
            info = JokeSocialBean(),
            joke = JokeContentBean(content = "哈哈哈哈哈哈哈哈哈哈"),
            user = JokeUser(nickName = "我是名字", signature = "我是签名！！！")
        )
    )
}

@Composable
private fun JokeItem(jokeBean: JokeTotalBean) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = FunnyJokeTheme.dimensions.paddingMedium)
            .background(color = Color.White)
            .padding(FunnyJokeTheme.dimensions.paddingMedium)
    ) {
        JokeUserHead(jokeBean.user)
        JokeContent(jokeBean)
        JokeToolBarBottom(jokeBean.info)
    }
}

@Composable
private fun JokeContent(jokeBean: JokeTotalBean) {
    val picList = remember(jokeBean.joke.imageUrl) {
        jokeBean.joke.imageUrl.split(",").map { it.funnyJokeDecrypt() }.filter { it.isNotEmpty() }
    }
    val videoUrlDecrypted = remember(jokeBean.joke.videoUrl) {
        jokeBean.joke.videoUrl.funnyJokeDecrypt()
    }
    Column(
        modifier = Modifier
            .padding(vertical = FunnyJokeTheme.dimensions.paddingMedium)
            .fillMaxWidth()
    ) {
        Text(
            text = jokeBean.joke.content,
            style = FunnyJokeTheme.typography.h3
        )
        if (picList.isNotEmpty()) {
            if (picList.size > 1) {
                JokeContentNineGridPic(picUrlList = picList)
            } else {
                JokeContentSinglePic(picUrl = picList.first())
            }
        }
        if (videoUrlDecrypted.isNotEmpty()) {
            VideoPlayer(url = videoUrlDecrypted)
        }
    }

}

@Composable
private fun JokeContentSinglePic(picUrl: String) {
    val giftDecoder = remember {
        GifDecoder.Factory()
    }
    Surface(
        shape = RoundedCornerShape(FunnyJokeTheme.dimensions.paddingMedium),
        border = BorderStroke(1.dp, Color.Gray.copy(alpha = 0.2f))
    ) {
        SubcomposeAsyncImage(
            modifier = Modifier
                .heightIn(min = 0.dp, max = 300.dp)
                .fillMaxHeight(),
            model = ImageRequest.Builder(LocalContext.current)
                .data(picUrl)
                .also {
                    if (picUrl.endsWith(".gif")) {
                        it.decoderFactory(giftDecoder)
                    }
                }
                .crossfade(true)
                .build(),
            contentDescription = null,
            contentScale = ContentScale.FillHeight
        ) {
            when (painter.state) {
                is AsyncImagePainter.State.Loading -> {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        CircularProgressIndicator(
                            modifier = Modifier.size(100.dp),
                            color = FunnyJokeTheme.colors.primary
                        )
                    }
                }

                is AsyncImagePainter.State.Error -> {
                    Image(
                        painter = painterResource(id = R.drawable.ic_picture_load_err),
                        contentDescription = null,
                        modifier = Modifier.fillMaxSize()
                    )
                }

                else -> {
                    SubcomposeAsyncImageContent()
                }
            }
        }
    }

}

@Composable
private fun JokeContentNineGridPic(picUrlList: List<String>) {
    VerticalGrid(modifier = Modifier.fillMaxWidth(), columns = 3) {
        picUrlList.forEach { url ->
            Surface(
                modifier = Modifier.padding(5.dp),
                shape = RoundedCornerShape(FunnyJokeTheme.dimensions.paddingSmall),
                border = BorderStroke(1.dp, Color.Gray.copy(alpha = 0.2f))
            ) {
                AsyncImage(
                    modifier = Modifier
                        .fillMaxSize()
                        .aspectRatio(1f / 1f),
                    model = url,
                    contentDescription = null,
                    contentScale = ContentScale.Crop
                )
            }
        }
    }
}

@Composable
@Preview
private fun PreviewJokeUserHead() {
    JokeUserHead(user = JokeUser(nickName = "哈哈嘻嘻嘻", signature = "我是签名签名"))
}

@Composable
private fun JokeUserHead(user: JokeUser) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Min),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Image(
            modifier = Modifier
                .size(40.dp)
                .clip(CircleShape),
            painter = rememberAsyncImagePainter(model = user.avatar),
            contentDescription = null
        )

        Spacer(modifier = Modifier.size(10.dp))

        Column(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .weight(1f, false),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = user.nickName,
                style = FunnyJokeTheme.typography.h3.copy(
                    color = Color.Black
                ),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = user.signature,
                style = FunnyJokeTheme.typography.h5.copy(
                    color = Color.Black.copy(alpha = 0.7f),
                ),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }

        Image(
            modifier = Modifier.size(20.dp),
            painter = painterResource(id = R.drawable.ic_more_dot),
            contentDescription = null
        )

    }
}

@Composable
private fun JokeToolBarBottom(
    jokeSocialBean: JokeSocialBean,
) {

    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceAround
    ) {
        JokeToolBarItem(
            drawableId = R.drawable.ic_like,
            content = jokeSocialBean.likeNum.toString()
        )
        JokeToolBarItem(
            drawableId = R.drawable.ic_dislike,
            content = jokeSocialBean.disLikeNum.toString()
        )
        JokeToolBarItem(
            drawableId = R.drawable.ic_comment,
            content = jokeSocialBean.commentNum.toString()
        )
        JokeToolBarItem(
            drawableId = R.drawable.ic_share,
            content = jokeSocialBean.shareNum.toString()
        )
    }

}

@Composable
private fun JokeToolBarItem(
    @DrawableRes drawableId: Int,
    content: String,
) {
    Row(
        modifier = Modifier.padding(vertical = FunnyJokeTheme.dimensions.paddingMedium),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Image(
            modifier = Modifier.size(20.dp),
            painter = painterResource(id = drawableId),
            contentDescription = null
        )
        Spacer(modifier = Modifier.size(5.dp))
        Text(text = content, style = FunnyJokeTheme.typography.h4)
    }
}