package com.sundayting.funnyjoke.function.home.tab.homepage.common

import com.sundayting.funnyjoke.function.home.tab.homepage.bean.JokeTotalBean
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf

data class JokeListState(
    val jokeList: ImmutableList<JokeTotalBean> = persistentListOf(),
    val refreshing: Boolean = false,
    val loadingMore: Boolean = false,
    val onLoad: (Boolean) -> Unit = {},
)