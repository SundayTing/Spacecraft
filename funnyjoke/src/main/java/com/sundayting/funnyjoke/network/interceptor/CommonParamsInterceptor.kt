package com.sundayting.funnyjoke.network.interceptor

import com.sundayting.funnyjoke.function.register.repo.UserDataStore
import com.sundayting.funnyjoke.repo.GlobalRepository
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

private const val METHOD_GET = "GET"
private const val METHOD_POST = "POST"

@Singleton
class CommonParamsInterceptor @Inject constructor(
    private val globalRepo: GlobalRepository,
    userDataStore: UserDataStore,
) : Interceptor {

    private val tokenFlow = userDataStore.tokenFlow

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (request.method == METHOD_POST) {
            val body = request.body
            if (body != null && body !is MultipartBody) {
                return try {
                    chain.proceed(
                        chain.request()
                            .newBuilder()
                            .apply {
                                generateCommonParams().forEach { entry ->
                                    addHeader(entry.key, entry.value)
                                }
                            }
                            .build()
                    )
                } catch (e: Exception) {
                    //发生异常，原路返回
                    chain.proceed(chain.request())
                }
            }
        }
        return chain.proceed(chain.request())
    }

    private fun generateCommonParams() = mapOf(
        "token" to tokenFlow.value,
        "uk" to globalRepo.deviceUid,
        "channel" to "cretin_open_api",
        "app" to "1.0.0;1;10",
        "device" to "HUAWEI;CDY-AN00",
        "project_token" to "BC06655ABF0746EBB0C904C1951AE313"
    )

}