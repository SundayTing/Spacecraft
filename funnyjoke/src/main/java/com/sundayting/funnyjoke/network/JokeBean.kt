package com.sundayting.funnyjoke.network

data class JokeBean<T>(
    val data: T?,
    val msg: String = "",
    val code: Int = 0,
)

fun <T> JokeBean<T>.isSuccess(): Boolean {
    return code == 200
}

fun <T> JokeBean<T>.isTokenExpire(): Boolean {
    return code == 202
}

