package com.sundayting.funnyjoke.network.module

import android.util.Log
import com.sundayting.funnyjoke.network.interceptor.CommonParamsInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object OkhttpModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class TimeOut

    @Provides
    @Singleton
    fun provideOkhttpClient(
        @TimeOut timeOut: Long,
        //公参拦截器
        commonParamsInterceptor: CommonParamsInterceptor,
        //日志拦截器
        loggingInterceptor: HttpLoggingInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(timeOut, TimeUnit.SECONDS)
            .connectTimeout(timeOut, TimeUnit.SECONDS)
            .writeTimeout(timeOut, TimeUnit.SECONDS)
            .addInterceptor(commonParamsInterceptor)
            .addNetworkInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideLogInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Log.d("网络请求日志", message) }.apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @TimeOut
    @Provides
    @Singleton
    fun provideTimeOutSeconds(): Long {
        return 10L
    }

}