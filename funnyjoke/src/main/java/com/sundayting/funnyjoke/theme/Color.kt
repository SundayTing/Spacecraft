package com.sundayting.funnyjoke.theme

import androidx.compose.ui.graphics.Color

val colorLightPrimary = Color(0xFF1E90FF)
val colorLightTextPrimary = Color(0xFF000000)
val colorLightTextSecondary = Color(0xFF6C727A)
val colorLightBackground = Color(0xFFF8F8FF)
val colorLightCardBackground = Color(0xFFFFFFFF)
val colorLightError = Color(0xFFD62222)

val colorDarkPrimary = Color(0xFF1E90FF)
val colorDarkTextPrimary = Color(0xFF000000)
val colorDarkTextSecondary = Color(0xFF6C727A)
val colorDarkBackground = Color(0xFFFFFFFF)
val colorDarkCardBackground = Color(0xFFF8F8FF)
val colorDarkError = Color(0xFFD62222)