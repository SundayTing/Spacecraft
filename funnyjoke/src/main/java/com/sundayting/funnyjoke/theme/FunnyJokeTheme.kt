package com.sundayting.funnyjoke.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Immutable
class FunnyJokeColor(
    primary: Color,
    background: Color,
    cardBackground: Color,
    textPrimary: Color,
    textSecondary: Color,
    isLight: Boolean,
) {

    var primary by mutableStateOf(primary)
        private set
    var background by mutableStateOf(background)
        private set
    var cardBackground by mutableStateOf(cardBackground)
        private set
    var textPrimary by mutableStateOf(textPrimary)
        private set
    var textSecondary by mutableStateOf(textSecondary)
        private set
    var isLight by mutableStateOf(isLight)

    fun copy(
        primary: Color = this.primary,
        background: Color = this.background,
        cardBackground: Color = this.cardBackground,
        textPrimary: Color = this.textPrimary,
        textSecondary: Color = this.textSecondary,
        isLight: Boolean = this.isLight,
    ) = FunnyJokeColor(
        primary = primary,
        background = background,
        cardBackground = cardBackground,
        textPrimary = textPrimary,
        textSecondary = textSecondary,
        isLight = isLight
    )

    fun update(other: FunnyJokeColor) {
        primary = other.primary
        background = other.background
        cardBackground = other.cardBackground
        textPrimary = other.textPrimary
        textSecondary = other.textSecondary
        isLight = other.isLight
    }

}

fun lightColors(
    primary: Color = colorLightPrimary,
    background: Color = colorLightBackground,
    cardBackground: Color = colorLightCardBackground,
    textPrimary: Color = colorLightTextPrimary,
    textSecondary: Color = colorLightTextSecondary,
) = FunnyJokeColor(
    primary = primary,
    background = background,
    cardBackground = cardBackground,
    textPrimary = textPrimary,
    textSecondary = textSecondary,
    isLight = true
)

fun darkColors(
    primary: Color = colorDarkPrimary,
    background: Color = colorDarkBackground,
    cardBackground: Color = colorDarkCardBackground,
    textPrimary: Color = colorDarkTextPrimary,
    textSecondary: Color = colorDarkTextSecondary,
) = FunnyJokeColor(
    primary = primary,
    background = background,
    cardBackground = cardBackground,
    textPrimary = textPrimary,
    textSecondary = textSecondary,
    isLight = false
)

val LocalColors = staticCompositionLocalOf { lightColors() }


data class AppTypography(
    val h1: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 24.sp
    ),
    val h2: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),
    val h3: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),
    val h4: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    val h5: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp
    ),
    val h6: TextStyle = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    ),
)

val LocalTypography = staticCompositionLocalOf { AppTypography() }

data class AppDimensions(
    val paddingSmall: Dp = 4.dp,
    val paddingMedium: Dp = 8.dp,
    val paddingLarge: Dp = 24.dp,
)

internal val LocalDimensions = staticCompositionLocalOf { AppDimensions() }

@Composable
fun FunnyJokeTheme(
    colors: FunnyJokeColor = FunnyJokeTheme.colors,
    typography: AppTypography = FunnyJokeTheme.typography,
    dimensions: AppDimensions = FunnyJokeTheme.dimensions,
    content: @Composable () -> Unit,
) {

    val rememberedColors = remember {
        colors.copy()
    }.apply { update(colors) }

    CompositionLocalProvider(
        LocalColors provides colors,
        LocalTypography provides typography,
        LocalDimensions provides dimensions,
    ) {
        val systemUiController = rememberSystemUiController()
        val useDarkIcons = !isSystemInDarkTheme()
        DisposableEffect(systemUiController, useDarkIcons) {
            systemUiController.setSystemBarsColor(
                color = Color.Transparent,
                darkIcons = useDarkIcons
            )
            onDispose {}
        }
        content()
    }
}

object FunnyJokeTheme {
    val colors: FunnyJokeColor
        @Composable
        @ReadOnlyComposable
        get() = LocalColors.current

    val typography: AppTypography
        @Composable
        @ReadOnlyComposable
        get() = LocalTypography.current

    val dimensions: AppDimensions
        @Composable
        @ReadOnlyComposable
        get() = LocalDimensions.current
}
