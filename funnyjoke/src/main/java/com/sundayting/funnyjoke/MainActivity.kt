package com.sundayting.funnyjoke

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.WindowCompat
import com.sundayting.funnyjoke.function.home.HomeScreen
import com.sundayting.funnyjoke.theme.FunnyJokeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            FunnyJokeTheme {
                HomeScreen()
            }
        }
    }
}
