package com.sundayting.funnyjoke.common.event

data class ToastEvent(val content: String) : OneShotEvent