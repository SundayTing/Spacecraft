package com.sundayting.funnyjoke.common.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUsers(vararg users: UserDetailBean)

    @Delete
    suspend fun deleteUsers(vararg users: UserDetailBean)

    @Query("SELECT * FROM UserDetailBean WHERE userId LIKE :id")
    fun queryUserById(id: Int): UserDetailBean?

    @Query("SELECT * FROM UserDetailBean WHERE userId LIKE :id")
    fun userFlowById(id: Int): Flow<UserDetailBean?>

}