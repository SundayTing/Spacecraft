package com.sundayting.funnyjoke.common

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

sealed interface LoadResult<out T> {
    data class Success<T>(val data: T) : LoadResult<T>
    data class Error(val exception: Throwable? = null) : LoadResult<Nothing>
    object Loading : LoadResult<Nothing>
}

fun <T> Flow<T>.asResult(): Flow<LoadResult<T>> {
    return this
        .map<T, LoadResult<T>> {
            LoadResult.Success(it)
        }
        .onStart { emit(LoadResult.Loading) }
        .catch { emit(LoadResult.Error(it)) }
}
