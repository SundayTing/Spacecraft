package com.sundayting.funnyjoke.common

import android.util.Base64
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

fun String.funnyJokeDecrypt(): String {
    if (!this.startsWith("ftp://") || this.isEmpty()) {
        return this
    }
    return try {
        // 生成密钥对象
        val secKey: SecretKey = SecretKeySpec("cretinzp**273846".toByteArray(), "AES")
        // 获取 AES 密码器
        val cipher = Cipher.getInstance("AES")
        // 初始化密码器（解密模型）
        cipher.init(Cipher.DECRYPT_MODE, secKey)
        // 解密数据, 返回明文
        cipher.doFinal(Base64.decode(this.replace("ftp://", "").toByteArray(), Base64.DEFAULT))
            .toString(Charset.forName("UTF-8"))
    } catch (e: Exception) {
        this
    }

}