package com.sundayting.funnyjoke.common.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sundayting.funnyjoke.common.db.dao.UserDao
import com.sundayting.funnyjoke.function.register.bean.UserDetailBean
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Database(entities = [UserDetailBean::class], version = 1)
abstract class FunnyJokeDB : RoomDatabase() {

    abstract fun userDao(): UserDao

}

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(
        @ApplicationContext context: Context,
    ): FunnyJokeDB {
        return Room
            .databaseBuilder(context, FunnyJokeDB::class.java, "funny-joke")
            .build()
    }

}