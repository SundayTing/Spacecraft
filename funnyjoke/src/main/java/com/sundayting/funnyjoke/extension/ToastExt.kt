package com.sundayting.funnyjoke.extension

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes

fun Context.toast(content: String, durationLong: Boolean = true) {
    Toast.makeText(this, content, if (durationLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
        .show()
}

fun Context.toast(@StringRes contentId: Int, durationLong: Boolean = true) {
    Toast.makeText(this, contentId, if (durationLong) Toast.LENGTH_LONG else Toast.LENGTH_SHORT)
        .show()
}