package com.sundayting

import android.app.Application
import com.sundayting.com.network.NResult
import com.sundayting.com.network.global.ExceptionMsgNativeLangTransformer
import com.sundayting.com.network.global.HaloInitializer
import com.sundayting.com.network.global.NetworkResultTransformer
import com.sundayting.com.network.ktx.isNSuccess
import com.sundayting.com.network.ktx.toNFailure
import com.sundayting.funnyjoke.network.JokeBean
import com.sundayting.funnyjoke.network.isSuccess
import com.sundayting.funnyjoke.repo.GlobalRepository
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@HiltAndroidApp
class FunnyJokeApplication : Application() {

    @Inject
    lateinit var globalRepository: GlobalRepository

    override fun onCreate() {
        super.onCreate()
        globalRepository.initDeviceUid()
        HaloInitializer.init(
            resultTransformer = object : NetworkResultTransformer {
                override fun <T> transformerBy(nResult: NResult<T>): NResult<T> {
                    //只关注成功的和酷麦实体类的情况
                    if (nResult.isNSuccess() && (nResult.body is JokeBean<*>)) {
                        val jokeBean = nResult.body as JokeBean<*>
                        //校验实体类的Error，如果发现是异常情况，则切换成异常类型返回值
                        if (!jokeBean.isSuccess()) {
                            return JokeStatusErrorException(jokeBean.msg).toNFailure()
                        }
                    }
                    return nResult
                }
            },
            nativeLangTransformer = object : ExceptionMsgNativeLangTransformer {
                override fun rawToNativeMsg(throwable: Throwable): String {
                    return if (throwable.isStatusErrorException()) {
                        throwable.msg
                    } else {
                        "网络错误"
                    }
                }

            }
        )
    }

}

/**
 * 后台网络异常
 */
class JokeStatusErrorException internal constructor(val msg: String) : Exception()

@OptIn(ExperimentalContracts::class)
fun Throwable.isStatusErrorException(): Boolean {
    contract {
        returns(true) implies (this@isStatusErrorException is JokeStatusErrorException)
    }
    return this is JokeStatusErrorException
}