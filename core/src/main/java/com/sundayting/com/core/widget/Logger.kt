package com.sundayting.com.core.widget

interface Logger {

    fun log(message: String)

}