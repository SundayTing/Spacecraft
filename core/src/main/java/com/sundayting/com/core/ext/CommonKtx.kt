package com.sundayting.com.core.ext

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

fun String.urlEncode(enc: String = StandardCharsets.UTF_8.toString()): String {
    return URLEncoder.encode(
        this,
        enc
    )
}