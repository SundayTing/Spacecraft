package com.sundayting.com.todayinhisgory

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.sundayting.com.network.global.HaloInitializer
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class HistoryApplication : Application(), Configuration.Provider {

    override fun onCreate() {
        super.onCreate()
        HaloInitializer.init()
    }

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
}