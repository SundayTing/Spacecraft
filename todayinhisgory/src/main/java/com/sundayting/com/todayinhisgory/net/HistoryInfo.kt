package com.sundayting.com.todayinhisgory.net

import kotlinx.serialization.Serializable

@Serializable
sealed interface HistoryInfo {

    @Serializable
    object Loading : HistoryInfo

    @Serializable
    data class Available(
        val data: DailyHistoryBean,
        val filePath: String,
        val color: Int
    ) : HistoryInfo

    @Serializable
    data class Unavailable(val message: String) : HistoryInfo
}