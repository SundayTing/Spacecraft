package com.sundayting.com.todayinhisgory.net

import android.content.Context
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import androidx.datastore.dataStoreFile
import androidx.glance.state.GlanceStateDefinition
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import java.io.File
import java.io.InputStream
import java.io.OutputStream

object HistoryStateDefinition : GlanceStateDefinition<HistoryInfo> {

    private const val DATA_STORE_FILENAME = "historyInfo"


    private val Context.datastore by dataStore(DATA_STORE_FILENAME, HistoryInfoSerializer)

    override suspend fun getDataStore(context: Context, fileKey: String): DataStore<HistoryInfo> {
        return context.datastore
    }

    override fun getLocation(context: Context, fileKey: String): File {
        return context.dataStoreFile(DATA_STORE_FILENAME)
    }

    object HistoryInfoSerializer : Serializer<HistoryInfo> {
        override val defaultValue = HistoryInfo.Loading

        override suspend fun readFrom(input: InputStream): HistoryInfo = try {
            Json.decodeFromString(
                HistoryInfo.serializer(),
                input.readBytes().decodeToString()
            )
        } catch (exception: SerializationException) {
            throw CorruptionException("Could not read weather data: ${exception.message}")
        }

        override suspend fun writeTo(t: HistoryInfo, output: OutputStream) {
            output.use {
                it.write(
                    Json.encodeToString(HistoryInfo.serializer(), t).encodeToByteArray()
                )
            }
        }
    }
}