package com.sundayting.com.todayinhisgory

import android.content.Context
import android.graphics.BitmapFactory
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.net.toUri
import androidx.glance.*
import androidx.glance.action.ActionParameters
import androidx.glance.appwidget.*
import androidx.glance.appwidget.action.ActionCallback
import androidx.glance.appwidget.action.actionRunCallback
import androidx.glance.layout.*
import androidx.glance.text.Text
import androidx.glance.text.TextAlign
import androidx.glance.text.TextStyle
import com.sundayting.com.todayinhisgory.net.HistoryInfo
import com.sundayting.com.todayinhisgory.net.HistoryStateDefinition
import com.sundayting.com.todayinhisgory.net.HistoryWorker


class TodayInHistoryGlanceWidgetReceiver : GlanceAppWidgetReceiver() {

    override val glanceAppWidget: GlanceAppWidget = TodayInHistoryGlanceWidget()

    override fun onEnabled(context: Context) {
        super.onEnabled(context)
        HistoryWorker.enqueue(context, periodic = true)
        HistoryWorker.enqueue(context, periodic = false)
    }

    override fun onDisabled(context: Context) {
        super.onDisabled(context)
        HistoryWorker.cancel(context)
    }

}


class TodayInHistoryGlanceWidget : GlanceAppWidget() {

    override val stateDefinition = HistoryStateDefinition

    @Composable
    override fun Content() {
        val historyInfo = currentState<HistoryInfo>()
        val backGroundColor = if (historyInfo is HistoryInfo.Available) {
            Color(historyInfo.color)
        } else {
            Color.White
        }
        Box(
            modifier = GlanceModifier
                .fillMaxSize()
                .background(day = backGroundColor, night = backGroundColor)
                .cornerRadius(10.dp),
            contentAlignment = Alignment.TopStart
        ) {
            when (historyInfo) {
                is HistoryInfo.Loading -> {
                    Text("加载中")
                }
                is HistoryInfo.Available -> {

                    Box(modifier = GlanceModifier.fillMaxSize()) {
                        Image(
                            provider = getImageProvider(historyInfo.filePath),
                            contentDescription = null,
                            contentScale = ContentScale.Fit,
                            modifier = GlanceModifier
                                .fillMaxSize()
                        )

                        Box(
                            modifier = GlanceModifier.fillMaxSize().padding(10.dp),
                            contentAlignment = Alignment.BottomEnd
                        ) {
                            Column(horizontalAlignment = Alignment.End) {
                                Text(
                                    modifier = GlanceModifier
                                        .padding(3.dp)
                                        .background(Color.White.copy(alpha = 0.8f)),
                                    text = "历史上的今天",
                                    style = TextStyle(
                                        fontSize = 16.sp,
                                    )
                                )
                                Text(
                                    modifier = GlanceModifier
                                        .padding(3.dp)
                                        .background(Color.White.copy(alpha = 0.8f)),
                                    text = historyInfo.data.title,
                                    style = TextStyle(
                                        fontSize = 14.sp,
                                        textAlign = TextAlign.End
                                    )
                                )
                                Text(
                                    "${historyInfo.data.year}年${historyInfo.data.month}月${historyInfo.data.day}日",
                                    modifier = GlanceModifier.padding(3.dp)
                                        .background(Color.White.copy(alpha = 0.8f)),
                                    style = TextStyle(
                                        fontSize = 14.sp,
                                        textAlign = TextAlign.End
                                    )
                                )

                            }
                        }

                    }
                }
                is HistoryInfo.Unavailable -> {
                    Button("加载数据", onClick = actionRunCallback<OnRefreshCallback>())
                }
            }
        }
    }

    private fun getImageProvider(path: String): ImageProvider {
        if (path.startsWith("content://")) {
            return ImageProvider(path.toUri())
        }
        val bitmap = BitmapFactory.decodeFile(path)
        return ImageProvider(bitmap)
    }

}

class OnRefreshCallback : ActionCallback {
    override suspend fun onAction(
        context: Context,
        glanceId: GlanceId,
        parameters: ActionParameters
    ) {
        HistoryWorker.enqueue(context)
    }

}