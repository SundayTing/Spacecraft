package com.sundayting.com.todayinhisgory.net

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import androidx.core.net.toUri
import androidx.glance.GlanceId
import androidx.glance.appwidget.GlanceAppWidgetManager
import androidx.glance.appwidget.state.updateAppWidgetState
import androidx.glance.appwidget.updateAll
import androidx.hilt.work.HiltWorker
import androidx.palette.graphics.Palette
import androidx.work.*
import coil.annotation.ExperimentalCoilApi
import coil.imageLoader
import coil.request.ErrorResult
import coil.request.ImageRequest
import com.sundayting.com.network.ktx.isNSuccess
import com.sundayting.com.todayinhisgory.R
import com.sundayting.com.todayinhisgory.TodayInHistoryGlanceWidget
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

@HiltWorker
class HistoryWorker @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted private val workerParameters: WorkerParameters,
    private val historyService: DailyHistoryService,
) : CoroutineWorker(context, workerParameters) {

    private val historyBeanList = mutableListOf<DailyHistoryBean>()

    private var loopJob: Job? = null

    companion object {

        private val uniqueWorkName = HistoryWorker::class.java.simpleName

        fun enqueue(context: Context, periodic: Boolean = true, force: Boolean = true) {
            val manager = WorkManager.getInstance(context)
            val a: Any = 3
            if (a is String) {
                a.toUri()
            }
            if (periodic) {
                manager.enqueueUniquePeriodicWork(
                    uniqueWorkName,
                    if (force) ExistingPeriodicWorkPolicy.REPLACE else ExistingPeriodicWorkPolicy.KEEP,
                    PeriodicWorkRequestBuilder<HistoryWorker>(
                        1, TimeUnit.DAYS,
                        15, TimeUnit.MINUTES
                    ).build()
                )
            } else {
                manager.enqueueUniqueWork(
                    uniqueWorkName,
                    if (force) ExistingWorkPolicy.REPLACE else ExistingWorkPolicy.REPLACE,
                    OneTimeWorkRequestBuilder<HistoryWorker>().build()
                )
            }

        }

        fun cancel(context: Context) {
            WorkManager.getInstance(context).cancelUniqueWork(uniqueWorkName)
        }
    }

    override suspend fun doWork(): Result {
        return withContext(Dispatchers.Main) {
            val manager = GlanceAppWidgetManager(context)
            val glanceIds = manager.getGlanceIds(TodayInHistoryGlanceWidget::class.java)
            setWidgetState(glanceIds, HistoryInfo.Loading)
            val result = historyService.getDayInHistory()
            if (result.isNSuccess()) {
                loopJob?.cancel()
                loopJob = launch {
                    while (true) {
                        result.body.data.orEmpty().filter { bean ->
                            bean.img.isNotEmpty()
                        }.shuffled().forEach { bean ->
                            try {
                                val path = getImagePath(bean.img)
                                setWidgetState(
                                    glanceIds, HistoryInfo.Available(
                                        bean,
                                        path,
                                        Palette.from(BitmapFactory.decodeFile(path)).generate()
                                            .getDarkVibrantColor(
                                                ResourcesCompat.getColor(
                                                    context.resources,
                                                    R.color.white,
                                                    null
                                                )
                                            )
                                    )
                                )
                            } catch (e: Exception) {
                                Log.d("临时测试", "出事了$e")
                            } finally {
                                delay(15 * 1000L)
                            }
                        }
                    }
                }
                Result.success()
            } else {
                setWidgetState(glanceIds, HistoryInfo.Unavailable("网络请求失败"))
                Result.failure()
            }
        }
    }

    @OptIn(ExperimentalCoilApi::class)
    private suspend fun getImagePath(url: String): String {
        val request = ImageRequest.Builder(context)
            .data(url)
            .build()

        // Request the image to be loaded and throw error if it failed
        with(context.imageLoader) {
            val result = execute(request)
            if (result is ErrorResult) {
                throw result.throwable
            }
        }

        // Get the path of the loaded image from DiskCache.
        val path = context.imageLoader.diskCache?.get(url)?.use { snapshot ->
            snapshot.data.toFile().path
        }
        return requireNotNull(path) {
            "Couldn't find cached file"
        }
    }

    private suspend fun setWidgetState(glanceIds: List<GlanceId>, historyInfo: HistoryInfo) {
        glanceIds.forEach { glanceId ->
            updateAppWidgetState(
                context = context,
                definition = HistoryStateDefinition,
                glanceId = glanceId,
                updateState = { historyInfo }
            )
        }
        TodayInHistoryGlanceWidget().updateAll(context)
    }
}