package com.sundayting.com.todayinhisgory.module

import android.util.Log
import com.sundayting.com.network.HaloCallAdapterFactory
import com.sundayting.com.todayinhisgory.BuildConfig
import com.sundayting.com.todayinhisgory.net.DailyHistoryService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ServiceModule {

    @Provides
    @Singleton
    fun provideService(
        retrofit: Retrofit
    ): DailyHistoryService {
        return retrofit.create(DailyHistoryService::class.java)
    }

}

@InstallIn(SingletonComponent::class)
@Module
object RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory,
        callAdapterFactory: CallAdapter.Factory,
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .baseUrl("https://dh.ylapi.cn/")
            .build()
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor { chain ->
                val originRequest = chain.request()
                if (originRequest.method == "GET") {
                    chain.proceed(
                        originRequest.newBuilder()
                            .url(
                                originRequest.url.newBuilder()
                                    .addQueryParameter("uid", BuildConfig.ghostuid)
                                    .addQueryParameter("appkey", BuildConfig.ghostappKey)
                                    .build()
                            )
                            .build()
                    )
                } else {
                    chain.proceed(chain.request())
                }
            }
            .addInterceptor(HttpLoggingInterceptor { message ->
                Log.d(
                    "网络日志",
                    message
                )
            }.apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): CallAdapter.Factory {
        return HaloCallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

}