package com.sundayting.com.todayinhisgory.net

import com.sundayting.com.network.NResult
import retrofit2.http.GET

interface DailyHistoryService {

    @GET("today_his/query.u")
    suspend fun getDayInHistory(): NResult<GhostBean<List<DailyHistoryBean>>>

}