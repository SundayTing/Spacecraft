package com.sundayting.com.todayinhisgory.net

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable

@Serializable
data class GhostBean<T>(
    val msg: String = "",
    val code: String = "",
    @SerializedName("datas") val data: T? = null,
)