package com.sundayting.com.todayinhisgory.net

import kotlinx.serialization.Serializable

@Serializable
data class DailyHistoryBean(
    val img: String = "",
    val title: String = "",
    val month: String = "",
    val year: String = "",
    val day: String = ""
)