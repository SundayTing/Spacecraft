package com.sundayting.wanandroid

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val items = listOf(
            Screen.ScreenA,
            Screen.ScreenB,
            Screen.ScreenC
        )
        setContent {
            val navController = rememberNavController()
            Scaffold(
                bottomBar = {
                    val navBackStackEntry by navController.currentBackStackEntryAsState()
                    val currentDestination = navBackStackEntry?.destination
                    if (currentDestination?.route == Screen.ScreenA.route || currentDestination?.route == Screen.ScreenB.route) {
                        BottomNavigation {
                            items.forEach { screen ->
                                BottomNavigationItem(
                                    icon = {
                                        Icon(
                                            Icons.Filled.Favorite,
                                            contentDescription = null
                                        )
                                    },
                                    label = { Text(text = screen.route) },
                                    selected = currentDestination.hierarchy.any { it.route == screen.route },
                                    onClick = {
                                        navController.navigate(screen.route) {
                                            // Pop up to the start destination of the graph to
                                            // avoid building up a large stack of destinations
                                            // on the back stack as users select items
                                            popUpTo(navController.graph.findStartDestination().id) {
                                                saveState = true
                                            }
                                            // Avoid multiple copies of the same destination when
                                            // reselecting the same item
                                            launchSingleTop = true
                                            // Restore state when reselecting a previously selected item
                                            restoreState = true
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            ) { innerPadding ->
                NavHost(
                    navController = navController,
                    startDestination = Screen.ScreenA.route,
                    modifier = Modifier.padding(innerPadding)
                ) {
                    composable(
                        route = Screen.ScreenA.route,
                        arguments = listOf(navArgument("content") { defaultValue = "A" })
                    ) {
                        ContentSample(
                            color = Color.Gray.copy(alpha = 0.5f),
                            onClick = {
                                navController.navigate(Screen.ScreenB.route)
                            })
                    }
                    composable(
                        route = Screen.ScreenB.route,
                        arguments = listOf(navArgument("content") { defaultValue = "B" })
                    ) {
                        ContentSample(
                            color = Color.Red.copy(alpha = 0.2f),
                            onClick = {
                                navController.navigate(Screen.ScreenA.route)
                            })
                    }
                    composable(
                        route = Screen.ScreenC.route,
                        arguments = listOf(navArgument("content") { defaultValue = "C" })
                    ) {
                        ContentSample(
                            color = Color.Green.copy(alpha = 0.2f),
                            onClick = {
                                navController.popBackStack()
                            })
                    }
                }
            }


        }
    }
}

@Composable
fun ContentSample(
    viewModel: ScreenViewModel = hiltViewModel(),
    color: Color,
    onClick: () -> Unit = {},
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color)
            .clickable { onClick() },
        contentAlignment = Alignment.Center
    ) {
        Text(viewModel.content)
    }
}

sealed class Screen(val route: String) {
    object ScreenA : Screen("content_A?content={content}")
    object ScreenB : Screen("content_B?content={content}")
    object ScreenC : Screen("content_C?content={content}")
}


