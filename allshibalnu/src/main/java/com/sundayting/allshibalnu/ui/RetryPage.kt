package com.sundayting.allshibalnu.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.sundayting.allshibalnu.R

@Composable
fun RetryPage(modifier: Modifier = Modifier, onRetry: () -> Unit) {
    Column(
        modifier = modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            modifier = Modifier.size(150.dp),
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = null
        )
        Text(
            "Ops！似乎来到了没有小柴的荒原",
            modifier = Modifier.padding(20.dp),
            fontSize = 20.sp
        )
        RetryButton(onRetry)
    }
}

@Composable
@Preview(widthDp = 400, heightDp = 800, showSystemUi = true, showBackground = true)
fun PreviewRetryPage() {
    RetryPage {

    }
}