package com.sundayting.allshibalnu.ui

import androidx.compose.animation.core.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.SubcomposeAsyncImage
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.sundayting.allshibalnu.R

@Composable
fun DogCard(url: String, modifier: Modifier = Modifier) {

    val infiniteTransition = rememberInfiniteTransition()
    val scale by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 0.8f,
        animationSpec = infiniteRepeatable(
            animation = tween(1000),
            repeatMode = RepeatMode.Reverse
        )
    )

    Card(
        modifier = modifier
            .fillMaxWidth()
            .aspectRatio(ratio = 1.0f),
        shape = RoundedCornerShape(15.dp),
        elevation = 5.dp,
        content = {
            SubcomposeAsyncImage(
                loading = {
                    Box(
                        modifier = Modifier
                            .padding(20.dp)
                            .scale(scale), contentAlignment = Alignment.Center
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_logo),
                            contentDescription = null
                        )
                    }
                },
                error = {
                    Box(
                        modifier = Modifier
                            .padding(20.dp)
                            .aspectRatio(ratio = 1f),
                        contentAlignment = Alignment.Center
                    ) {
                        Column(horizontalAlignment = Alignment.CenterHorizontally) {
                            Image(
                                modifier = Modifier.size(40.dp),
                                painter = painterResource(id = R.drawable.ic_dog_dead),
                                contentDescription = null
                            )
                            Text("狗带了", fontSize = 20.sp)
                        }
                    }
                },
                contentScale = ContentScale.Crop,
                model = ImageRequest.Builder(LocalContext.current)
                    .data(url)
                    .crossfade(true)
                    .diskCachePolicy(CachePolicy.ENABLED)
                    .build(),
                contentDescription = null,
            )
        }
    )
}

@Composable
@Preview(showBackground = true)
fun PreviewDogCard() {

    DogCard(url = "https://cdn.shibe.online/shibes/abd8f639e95024f794a9411ac079a6048c024ea9.jpg")

}