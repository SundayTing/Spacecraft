package com.sundayting.allshibalnu.ui

import androidx.compose.foundation.layout.Row
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun RetryButton(
    onClick: () -> Unit = {}
) {

    Button(
        onClick = onClick, colors = ButtonDefaults.buttonColors(
            backgroundColor = Color(0xFFFFb84d),
            contentColor = Color.White
        )
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(Icons.Rounded.Refresh, contentDescription = null)
            Text("重新捕获小柴")
        }
    }

}

@Composable
@Preview
fun PreviewRetryButton() {
    RetryButton()
}