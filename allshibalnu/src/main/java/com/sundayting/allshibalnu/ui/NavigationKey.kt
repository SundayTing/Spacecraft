package com.sundayting.allshibalnu.ui

object NavigationKey {

    const val LIST = "LIST"
    const val CONTENT = "CONTENT"

}