package com.sundayting.allshibalnu

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.sundayting.allshibalnu.page.content_page.ContentPage
import com.sundayting.allshibalnu.page.list_page.ListPage
import com.sundayting.allshibalnu.ui.NavigationKey
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val systemUiController = rememberSystemUiController()
            DisposableEffect(systemUiController) {

                systemUiController.setStatusBarColor(
                    color = Color(0xFFFFb84d),
                    darkIcons = false
                )

                onDispose { }
            }

            val navController = rememberNavController()

            NavHost(navController = navController, startDestination = NavigationKey.LIST) {
                composable(NavigationKey.LIST) { ListPage(navController) }
                composable(
                    "${NavigationKey.CONTENT}/{url}",
                    arguments = listOf(navArgument("url") { type = NavType.StringType })
                ) { ContentPage(navController) }
            }


        }
    }
}

