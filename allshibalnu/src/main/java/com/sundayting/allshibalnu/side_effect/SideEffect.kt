package com.sundayting.allshibalnu.side_effect

interface SideEffect

class ToastSideEffect(val text: String) : SideEffect {

    override fun toString(): String {
        return text
    }

}