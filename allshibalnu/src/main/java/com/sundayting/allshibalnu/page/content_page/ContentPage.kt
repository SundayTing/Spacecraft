package com.sundayting.allshibalnu.page.content_page

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import com.sundayting.allshibalnu.R
import com.sundayting.allshibalnu.side_effect.ToastSideEffect
import com.sundayting.com.core.ext.toast

@Composable
fun ContentPage(
    navController: NavController = rememberNavController(),
    viewModel: ContentPageViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiStateFlow.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(viewModel) {
        viewModel.sideEffect.collect { sideEffect ->
            if (sideEffect is ToastSideEffect) {
                context.toast(sideEffect.toString())
            }
        }
    }
    Scaffold { paddingValue ->
        Surface(modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .padding(paddingValue)
            .pointerInput(Unit) {
                detectTapGestures(onTap = {
                    navController.popBackStack()
                })
            }) {
            Box(
                modifier = Modifier
                    .background(Color(0xffffae00).copy(0.2f))
            ) {
                Column(
                    modifier = Modifier
                        .align(Alignment.Center),
                    verticalArrangement = Arrangement.Center
                ) {
                    AsyncImage(
                        modifier = Modifier
                            .fillMaxWidth(),
                        model = uiState.imageUrl,
                        contentDescription = null,
                        contentScale = ContentScale.FillWidth,
                        onSuccess = { success ->
                            viewModel.onLoadSuccess(success.result.drawable)
                        }
                    )
                }

                if (uiState.imageDrawable != null && !uiState.saveSuccess)
                    Image(
                        modifier = Modifier
                            .align(Alignment.BottomCenter)
                            .padding(bottom = 50.dp)
                            .size(50.dp)
                            .background(Color.Transparent)
                            .clickable {
                                viewModel.savePic()
                            },
                        painter = painterResource(R.drawable.ic_save),
                        contentDescription = null
                    )
                Box(modifier = Modifier.size(50.dp))


            }


        }
    }

}