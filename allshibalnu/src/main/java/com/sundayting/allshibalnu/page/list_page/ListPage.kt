package com.sundayting.allshibalnu.page.list_page

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.loren.component.view.composesmartrefresh.*
import com.sundayting.allshibalnu.ui.DogCard
import com.sundayting.allshibalnu.ui.NavigationKey
import com.sundayting.allshibalnu.ui.RetryPage
import com.sundayting.com.core.ext.urlEncode

@Composable
fun ListPage(
    navController: NavController = rememberNavController(),
    viewModel: ListPageViewModel = hiltViewModel()
) {

    val uiState by viewModel.uiStateFlow.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = Color(0xFFFFb84d),
                elevation = 0.dp
            ) {
                Text(
                    "全是柴犬",
                    color = Color.White,
                    fontSize = 20.sp,
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            }
        },
    ) { paddingValues ->
        val refreshState = rememberSmartSwipeRefreshState()
        val lazyGridState = rememberLazyGridState()
        if (uiState.needRetry) {
            RetryPage(modifier = Modifier.fillMaxHeight()) { viewModel.refreshDog() }
        } else {
            SmartSwipeRefresh(
                state = refreshState,
                onRefresh = { viewModel.refreshDog() },
                onLoadMore = { viewModel.loadMoreDog() },
                headerIndicator = {
                    MyRefreshHeader(refreshState.refreshFlag, true)
                },
                footerIndicator = {
                    MyRefreshFooter(refreshState.loadMoreFlag, true)
                }
            ) {

                LaunchedEffect(refreshState.smartSwipeRefreshAnimateFinishing) {
                    if (refreshState.smartSwipeRefreshAnimateFinishing.isFinishing && !refreshState.smartSwipeRefreshAnimateFinishing.isRefresh) {
                        lazyGridState.animateScrollToItem(lazyGridState.firstVisibleItemIndex + 1)
                    }
                }

                LaunchedEffect(uiState.loadSuccess, uiState.refreshSuccess, uiState.loading) {
                    if (!uiState.loading) {
                        refreshState.refreshFlag = when (uiState.refreshSuccess) {
                            true -> SmartSwipeStateFlag.SUCCESS
                            false -> SmartSwipeStateFlag.ERROR
                            else -> SmartSwipeStateFlag.IDLE
                        }
                        refreshState.loadMoreFlag = when (uiState.loadSuccess) {
                            true -> SmartSwipeStateFlag.SUCCESS
                            false -> SmartSwipeStateFlag.ERROR
                            else -> SmartSwipeStateFlag.IDLE
                        }
                    }
                }

                LazyVerticalGrid(
                    state = lazyGridState,
                    modifier = Modifier.padding(paddingValues),
                    columns = GridCells.Fixed(3),
                ) {
                    items(uiState.dogList, key = {
                        it
                    }) { url ->
                        DogCard(url = url, modifier = Modifier
                            .padding(6.dp)
                            .clickable {
                                navController.navigate(
                                    "${NavigationKey.CONTENT}/${
                                        url.urlEncode()
                                    }"
                                )
                            })
                    }
                }
            }
        }

    }

}

