package com.sundayting.allshibalnu.page.list_page

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.allshibalnu.net.DogService
import com.sundayting.com.network.ktx.isNSuccess
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListPageViewModel @Inject constructor(
    private val dogService: DogService,
) : ViewModel() {

    private var getDogJobs: Job? = null

    private val _uiStateFlow = MutableStateFlow(UiState())
    val uiStateFlow = _uiStateFlow.asStateFlow()

    data class UiState(
        val dogList: List<String> = emptyList(),
        val loading: Boolean = false,
        val loadSuccess: Boolean = false,
        val refreshSuccess: Boolean = false,
        val needRetry: Boolean = false
    )

    init {
        getDog()
    }

    fun refreshDog() {
        getDog()
    }

    fun loadMoreDog() {
        getDog(false)
    }

    private fun getDog(refresh: Boolean = true) {

        getDogJobs?.cancel()
        getDogJobs = viewModelScope.launch {
            _uiStateFlow.update {
                it.copy(
                    needRetry = false,
                    loading = true
                )
            }
            val result = dogService.getDog()
            if (result.isNSuccess()) {
                _uiStateFlow.update { it ->
                    it.copy(
                        dogList = (
                                (if (refresh)
                                    emptyList()
                                else
                                    _uiStateFlow.value.dogList) + result.body
                                ).distinctBy { url -> url },
                        refreshSuccess = if (refresh) true else _uiStateFlow.value.refreshSuccess,
                        loadSuccess = if (!refresh) true else _uiStateFlow.value.loadSuccess,
                    )
                }
            } else {
                _uiStateFlow.update {
                    it.copy(
                        needRetry = _uiStateFlow.value.dogList.isEmpty(),
                        refreshSuccess = if (refresh) false else _uiStateFlow.value.refreshSuccess,
                        loadSuccess = if (!refresh) false else _uiStateFlow.value.loadSuccess,
                    )
                }
            }
            _uiStateFlow.update {
                it.copy(
                    loading = false
                )
            }
        }
    }

}