package com.sundayting.allshibalnu.page.content_page

import android.app.Application
import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.allshibalnu.side_effect.SideEffect
import com.sundayting.allshibalnu.side_effect.ToastSideEffect
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ContentPageViewModel @Inject constructor(
    private val application: Application,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {


    private var saveJob: Job? = null

    private val _uiStateFlow = MutableStateFlow(UiState())
    val uiStateFlow = _uiStateFlow.asStateFlow()

    private val _sideEffectFlow = MutableSharedFlow<SideEffect>()
    val sideEffect = _sideEffectFlow.asSharedFlow()
    private val uuid = UUID.randomUUID()

    data class UiState(
        val imageUrl: String? = null,
        val imageDrawable: Drawable? = null,
        val saveSuccess: Boolean = false
    )


    fun savePic() {
        if (saveJob?.isActive == true) {
            return
        }
        saveJob = viewModelScope.launch(Dispatchers.IO) {
            try {
                val displayName = "shibaInu$uuid"
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                    put(MediaStore.MediaColumns.DISPLAY_NAME, "shibaInu$displayName")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                    } else {
                        put(
                            MediaStore.MediaColumns.DATA,
                            "${Environment.getExternalStorageDirectory().path}/${Environment.DIRECTORY_PICTURES}/$displayName"
                        )
                    }

                }
                val uri = application.contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues
                )
                if (uri != null) {
                    val outputStream = application.contentResolver.openOutputStream(uri)
                    if (outputStream != null) {
                        uiStateFlow.value.imageDrawable!!.toBitmap()
                            .compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                        outputStream.close()
                    }
                }
                _sideEffectFlow.emit(ToastSideEffect("保存成功，请在相册中查看"))
                _uiStateFlow.update { it.copy(saveSuccess = true) }
            } catch (e: Exception) {
                _sideEffectFlow.emit(ToastSideEffect("保存失败！"))
            }
        }

    }

    fun onLoadSuccess(drawable: Drawable) {
        _uiStateFlow.update { it.copy(imageDrawable = drawable) }
    }

    init {
        _uiStateFlow.update { it.copy(imageUrl = savedStateHandle.get<String>("url")) }
    }

}