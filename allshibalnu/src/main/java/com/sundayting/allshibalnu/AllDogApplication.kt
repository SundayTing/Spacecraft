package com.sundayting.allshibalnu

import android.app.Application
import com.sundayting.com.network.global.HaloInitializer
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AllDogApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        HaloInitializer.init()
    }

}