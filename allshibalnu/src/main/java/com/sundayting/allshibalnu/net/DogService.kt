package com.sundayting.allshibalnu.net

import com.sundayting.com.network.NResult
import retrofit2.http.GET

interface DogService {

    @GET("api/shibes?count=100")
    suspend fun getDog(): NResult<List<String>>

}
