package com.sundayting.com.ui.mvi.extension


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.com.ui.mvi.ContainerLazy
import com.sundayting.com.ui.mvi.MutableContainer
import com.sundayting.com.ui.mvi.UiEvent
import com.sundayting.com.ui.mvi.UiState

/**
 * 构建viewModel的Ui容器，存储Ui状态和一次性事件
 */
fun <STATE : UiState> ViewModel.containers(
    initialState: STATE,
): Lazy<MutableContainer<STATE, UiEvent>> {
    return ContainerLazy(initialState, viewModelScope)
}