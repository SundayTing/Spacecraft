package com.sundayting.com.ui.mvi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sundayting.com.ui.mvi.extension.containers
import kotlinx.coroutines.launch

class TestViewModel : ViewModel() {

    data class TestUiState(
        val name: String = "",
    ) : UiState

    data class ToastEvent(
        val content: String = "",
    ) : UiEvent

    val container by containers(TestUiState())

    fun test() {

        //更新uiState
        container.updateState {
            copy(name = "test")
        }

        //发送事件
        container.sendEvent(ToastEvent("哈哈"))

        viewModelScope.launch {


            launch {
                container.uiStateFlow.collect {

                }
            }

            launch {
                container.singleEventFlow.collect {

                }
            }


        }


    }

}